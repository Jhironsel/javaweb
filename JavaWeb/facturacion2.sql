-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Mar 05, 2015 at 02:09 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `facturacion`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `clientes`
-- 

CREATE TABLE `clientes` (
  `idCliente` varchar(13) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `direccion` varchar(50) default NULL,
  `telefono` varchar(14) default NULL,
  `idCiudad` int(11) NOT NULL,
  `fechaNacimiento` date default NULL,
  `fechaIngreso` date default NULL,
  `credito` float NOT NULL,
  `deudaActual` float default NULL,
  PRIMARY KEY  (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `clientes`
-- 

INSERT INTO `clientes` VALUES ('0', 3, 'Cliente Generico Nombre', 'Y Apellidos', 'Cliente Generico Direccion', '0-000-000-0000', 1, '2013-11-05', '2013-11-05', 0, 0);
INSERT INTO `clientes` VALUES ('011-0048599-3', 1, 'MANUEL EMILIO', 'FERNANDEZ RODRIGUEZ', 'JOSE CONTRERA CASA #24', '829-443-5933', 2, '1975-10-11', '2013-10-29', 5000, 0);
INSERT INTO `clientes` VALUES ('012-0046244-8', 1, 'SILVIA SOPHIA', 'ALMONTE PEREZ', '27 FEBRERO', '809-135-4622', 3, '1978-10-02', '2013-10-25', 2345, 0);
INSERT INTO `clientes` VALUES ('012-0068755-5', 1, 'PEDRO PABLO', 'GUZMAN GONZALES', 'CALLE DUARTE CASA NUMERO #49', '849-945-4009', 3, '1977-12-31', '2013-10-30', 3500, 0);
INSERT INTO `clientes` VALUES ('012-0089344-1', 1, 'SOPHIA', 'DIAZ VARGAS', 'CALLE 27 DE FEBRERO', '809-975-2211', 2, '2006-01-22', '2013-10-22', 2000, 0);
INSERT INTO `clientes` VALUES ('012-0089344-2', 1, 'JHIRONSEL', 'DIAZ ALMONTE', 'CIRCUNV. SUR EDIF. 41 APART.102, Crito Rey.', '809-975-3152', 1, '1984-04-07', '2013-10-22', 3765, 0);
INSERT INTO `clientes` VALUES ('012-0089344-3', 1, 'GALFONCEL', 'DIAZ ALMONTE', 'CIRCUNV. SUR EDIF. 40 APART. 102', '809-975-3433', 3, '1986-06-13', '2013-10-24', 500.01, 76.99);
INSERT INTO `clientes` VALUES ('012-0097346-8', 1, 'PERLA MARIA', 'CABRERA NIN', 'PUENTE DUARTE CASA #23', '849-699-8997', 3, '2013-11-01', '2013-11-01', 128.48, 371.52);
INSERT INTO `clientes` VALUES ('022-0086955-3', 1, 'GUILLERMO', 'MORENO', 'SANCHEZ CASA #99', '809-245-4490', 4, '2013-10-29', '2013-10-29', 4000, 0);
INSERT INTO `clientes` VALUES ('edesur', 3, 'Paola', 'ramirez', 'anacaona  numero 24', '809-659-2222', 4, '1977-11-01', '2013-11-11', 2000, 0);
INSERT INTO `clientes` VALUES ('infotep', 3, 'Infotep Regional', 'San Juan', 'Salida de la Ciudad', '809-888-3333', 3, '1976-05-05', '2005-05-17', 1500, 0);
INSERT INTO `clientes` VALUES ('Navidad', 3, 'Programador', 'Activo', 'No se Duerme con esto', '794-223-4454', 4, '1994-12-09', '2013-12-23', 0, 0);
INSERT INTO `clientes` VALUES ('Pruebas', 3, 'Probando', 'Fecha Esta', 'No se', '999-999-9999', 1, '2001-12-20', '2013-12-10', 0, 0);
INSERT INTO `clientes` VALUES ('Urania Montas', 3, 'Centro Urania Montas', 'Montas', 'Colon a lado del Liceo Tec. PHU', '809-557-1223', 1, '2013-11-01', '2013-11-11', 4797.6, 202.4);

-- --------------------------------------------------------

-- 
-- Table structure for table `detallefactura`
-- 

CREATE TABLE `detallefactura` (
  `idFactura` int(11) NOT NULL,
  `idLinea` int(11) NOT NULL,
  `idProducto` varchar(24) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `cantidad` float NOT NULL,
  PRIMARY KEY  (`idFactura`,`idLinea`),
  KEY `idLinea` (`idLinea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `detallefactura`
-- 

INSERT INTO `detallefactura` VALUES (3, 0, '1', 'Coca Cola x 350 ml', 41, 2);
INSERT INTO `detallefactura` VALUES (4, 0, '1', 'Coca Cola x 350 ml', 41, 3);
INSERT INTO `detallefactura` VALUES (4, 1, '2', 'Jabon de Cuaba', 23, 4);
INSERT INTO `detallefactura` VALUES (4, 2, '22', 'XXX', 23, 2);
INSERT INTO `detallefactura` VALUES (5, 0, '1', 'Coca Cola x 350 ml', 41, 3);
INSERT INTO `detallefactura` VALUES (5, 1, '2', 'Jabon de Cuaba', 23, 4);
INSERT INTO `detallefactura` VALUES (6, 0, '22', 'XXX', 23, 2);
INSERT INTO `detallefactura` VALUES (7, 0, '4', 'LOLA BIEN', 513, 3);
INSERT INTO `detallefactura` VALUES (7, 1, '234', 'PRODUCTO', 43, 4);
INSERT INTO `detallefactura` VALUES (8, 0, '22', 'XXX', 23, 4);
INSERT INTO `detallefactura` VALUES (8, 1, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 2);
INSERT INTO `detallefactura` VALUES (9, 0, '1', 'Coca Cola x 350 ml', 41, 3);
INSERT INTO `detallefactura` VALUES (9, 1, '234', 'PRODUCTO', 43, 2);
INSERT INTO `detallefactura` VALUES (9, 2, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 5);
INSERT INTO `detallefactura` VALUES (10, 0, '2', 'Jabon de Cuaba', 23, 2);
INSERT INTO `detallefactura` VALUES (10, 1, '4', 'LOLA BIEN', 513, 3);
INSERT INTO `detallefactura` VALUES (11, 0, '000', 'PRODUCTO CON COSTO', 34, 4);
INSERT INTO `detallefactura` VALUES (13, 0, '4443', 'HARINA DEL NEGRITO', 43.44, 22.33);
INSERT INTO `detallefactura` VALUES (13, 1, '22', 'XXX', 23, 2.3);
INSERT INTO `detallefactura` VALUES (14, 0, '4', 'LOLA BIEN', 513, 22.33);
INSERT INTO `detallefactura` VALUES (15, 0, '4443', 'HARINA DEL NEGRITO', 43.44, 44.33);
INSERT INTO `detallefactura` VALUES (15, 1, '8599', 'ENSALADA LIB. 1', 34, 22.33);
INSERT INTO `detallefactura` VALUES (15, 2, '2', 'Jabon de Cuaba', 23, 54.44);
INSERT INTO `detallefactura` VALUES (15, 3, '22', 'XXX', 23, 56.55);
INSERT INTO `detallefactura` VALUES (16, 0, '2', 'Jabon de Cuaba', 23, 33.44);
INSERT INTO `detallefactura` VALUES (16, 1, '22', 'XXX', 23, 44);
INSERT INTO `detallefactura` VALUES (16, 2, '2', 'Jabon de Cuaba', 23, 22.33);
INSERT INTO `detallefactura` VALUES (17, 0, '2', 'Jabon de Cuaba', 23, 33.44);
INSERT INTO `detallefactura` VALUES (17, 1, '2', 'Jabon de Cuaba', 23, 333);
INSERT INTO `detallefactura` VALUES (17, 2, '2', 'Jabon de Cuaba', 23, 334);
INSERT INTO `detallefactura` VALUES (17, 3, '000', 'PRODUCTO CON COSTO', 34.22, 33344);
INSERT INTO `detallefactura` VALUES (17, 4, '4443', 'HARINA DEL NEGRITO', 43.44, 33433);
INSERT INTO `detallefactura` VALUES (18, 0, '000', 'PRODUCTO CON COSTO', 34.22, 22.33);
INSERT INTO `detallefactura` VALUES (18, 1, '84342', 'COMO VA ESTO', 36, 0.44);
INSERT INTO `detallefactura` VALUES (19, 0, '2', 'Jabon de Cuaba', 23, 22.33);
INSERT INTO `detallefactura` VALUES (20, 0, '22', 'XXX', 23, 23.22);
INSERT INTO `detallefactura` VALUES (21, 0, '84342', 'COMO VA ESTO', 36, 22.33);
INSERT INTO `detallefactura` VALUES (21, 1, '8934', 'AZUCAR PRIETA LIBRA', 23.5, 23.5);
INSERT INTO `detallefactura` VALUES (21, 2, '9844', 'PRODUCTO PARA ESTADO', 32.43, 22.5);
INSERT INTO `detallefactura` VALUES (22, 0, '4443', 'HARINA DEL NEGRITO', 43.44, 2.5);
INSERT INTO `detallefactura` VALUES (23, 0, '234', 'PRODUCTO', 43, 22);
INSERT INTO `detallefactura` VALUES (23, 1, '84342', 'COMO VA ESTO', 36, 22);
INSERT INTO `detallefactura` VALUES (23, 2, '8934', 'AZUCAR PRIETA LIBRA', 23.5, 22);
INSERT INTO `detallefactura` VALUES (24, 0, '433jjj', 'Cuando terminar', 33.4, 2);
INSERT INTO `detallefactura` VALUES (24, 1, '8599', 'ENSALADA LIB. 1', 34, 3.3);
INSERT INTO `detallefactura` VALUES (24, 2, '9844', 'PRODUCTO PARA ESTADO', 32.43, 33.4);
INSERT INTO `detallefactura` VALUES (25, 0, '433jjj', 'Cuando terminar', 33.4, 6);
INSERT INTO `detallefactura` VALUES (25, 1, '433jjj', 'Cuando terminar', 33.4, 8);
INSERT INTO `detallefactura` VALUES (25, 2, '84342', 'COMO VA ESTO', 36, 8);
INSERT INTO `detallefactura` VALUES (25, 3, '9844', 'PRODUCTO PARA ESTADO', 32.43, 8.7);
INSERT INTO `detallefactura` VALUES (26, 0, '433', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 7);
INSERT INTO `detallefactura` VALUES (26, 1, '433', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 7.8);
INSERT INTO `detallefactura` VALUES (27, 0, '234', 'PRODUCTO', 43, 4);
INSERT INTO `detallefactura` VALUES (27, 1, '4', 'LOLA BIEN', 513, 1);
INSERT INTO `detallefactura` VALUES (27, 2, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 1);
INSERT INTO `detallefactura` VALUES (27, 3, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 2.3);
INSERT INTO `detallefactura` VALUES (27, 4, '9844', 'PRODUCTO PARA ESTADO', 32.43, 2.3);
INSERT INTO `detallefactura` VALUES (27, 5, '433', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 2.4);
INSERT INTO `detallefactura` VALUES (27, 6, '2', 'Jabon de Cuaba', 23, 22.3);
INSERT INTO `detallefactura` VALUES (27, 7, '9844', 'PRODUCTO PARA ESTADO', 32.43, 0.98);
INSERT INTO `detallefactura` VALUES (28, 0, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (28, 1, '234', 'PRODUCTO', 43, 2.3);
INSERT INTO `detallefactura` VALUES (28, 2, '4', 'LOLA BIEN', 513, 3.4);
INSERT INTO `detallefactura` VALUES (29, 0, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (29, 1, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 2.2);
INSERT INTO `detallefactura` VALUES (30, 0, '000', 'PRODUCTO CON COSTO', 35.22, 4);
INSERT INTO `detallefactura` VALUES (30, 1, '000', 'PRODUCTO CON COSTO', 35.22, 4.4);
INSERT INTO `detallefactura` VALUES (30, 2, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (30, 3, '2', 'Jabon de Cuaba', 23, 22);
INSERT INTO `detallefactura` VALUES (30, 4, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 22);
INSERT INTO `detallefactura` VALUES (30, 5, '84342', 'COMO VA ESTO', 36, 12);
INSERT INTO `detallefactura` VALUES (30, 6, '9844', 'PRODUCTO PARA ESTADO', 32.43, 3.3);
INSERT INTO `detallefactura` VALUES (31, 0, '000', 'PRODUCTO CON COSTO', 35.22, 1);
INSERT INTO `detallefactura` VALUES (32, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (33, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (34, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (35, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (36, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (37, 0, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (38, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (39, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (40, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (41, 0, '234', 'PRODUCTO', 43, 2);
INSERT INTO `detallefactura` VALUES (42, 0, '234', 'PRODUCTO', 43, 2);
INSERT INTO `detallefactura` VALUES (43, 0, '000', 'PRODUCTO CON COSTO', 35.22, 2);
INSERT INTO `detallefactura` VALUES (44, 0, '000', 'PRODUCTO CON COSTO', 35.22, 1.33);
INSERT INTO `detallefactura` VALUES (44, 1, '1', 'COCA COLA X 350 ML', 41.48, 5.33);
INSERT INTO `detallefactura` VALUES (44, 2, '2', 'Jabon de Cuaba', 23, 3.99);
INSERT INTO `detallefactura` VALUES (44, 3, '22', 'XXX', 23.45, 3.33);
INSERT INTO `detallefactura` VALUES (44, 4, '433', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 6.44);
INSERT INTO `detallefactura` VALUES (45, 0, '000', 'PRODUCTO CON COSTO', 35.22, 1);
INSERT INTO `detallefactura` VALUES (45, 1, '000', 'PRODUCTO CON COSTO', 35.22, 1);
INSERT INTO `detallefactura` VALUES (46, 0, '9844', 'PRODUCTO PARA ESTADO', 32.43, 35);
INSERT INTO `detallefactura` VALUES (46, 1, '234', 'PRODUCTO', 43, 14);
INSERT INTO `detallefactura` VALUES (47, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (48, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (49, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (50, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (51, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (52, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (53, 0, '1', 'COCA COLA X 350 ML', 41.48, 1);
INSERT INTO `detallefactura` VALUES (54, 0, '1', 'COCA COLA X 350 ML', 41.48, 1);
INSERT INTO `detallefactura` VALUES (55, 0, '22', 'XXX', 23.45, 1);
INSERT INTO `detallefactura` VALUES (56, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (57, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (58, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (59, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (60, 0, '2', 'Jabon de Cuaba', 23, 2);
INSERT INTO `detallefactura` VALUES (61, 0, '2', 'Jabon de Cuaba', 23, 3);
INSERT INTO `detallefactura` VALUES (62, 0, '2', 'Jabon de Cuaba', 23, 2);
INSERT INTO `detallefactura` VALUES (63, 0, '1', 'COCA COLA X 350 ML', 41.48, 1);
INSERT INTO `detallefactura` VALUES (64, 0, '1', 'COCA COLA X 350 ML', 41.48, 1);
INSERT INTO `detallefactura` VALUES (65, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (65, 1, '234', 'PRODUCTO', 43, 3);
INSERT INTO `detallefactura` VALUES (66, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (67, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (68, 0, '2', 'Jabon de Cuaba', 23, 1);
INSERT INTO `detallefactura` VALUES (69, 0, '2', 'Jabon de Cuaba', 23, 5);
INSERT INTO `detallefactura` VALUES (70, 0, '234', 'PRODUCTO', 43, 0.8);
INSERT INTO `detallefactura` VALUES (70, 1, '2', 'Jabon de Cuaba', 23, 1.8);
INSERT INTO `detallefactura` VALUES (70, 2, '84342', 'COMO VA ESTO', 36, 0.03);
INSERT INTO `detallefactura` VALUES (71, 0, '22', 'XXX', 23.45, 3);
INSERT INTO `detallefactura` VALUES (72, 0, '4', 'LOLA BIEN', 513, 1);
INSERT INTO `detallefactura` VALUES (73, 0, '1', 'COCA COLA X 350 ML', 41.48, 3);
INSERT INTO `detallefactura` VALUES (74, 0, '22', 'XXX', 23.45, 1);
INSERT INTO `detallefactura` VALUES (75, 0, '1', 'COCA COLA X 350 ML', 41.48, 1);
INSERT INTO `detallefactura` VALUES (76, 1, '048231294461', 'DVD-R de 4.7 GB 8X', 16, 1);
INSERT INTO `detallefactura` VALUES (76, 2, '2', 'Jabon de Cuaba', 23, 7);
INSERT INTO `detallefactura` VALUES (78, 0, '2', 'Jabon de Cuaba', 23, 3);
INSERT INTO `detallefactura` VALUES (79, 0, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (80, 0, '2', 'Jabon de Cuaba', 23, 2);
INSERT INTO `detallefactura` VALUES (81, 1, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 2);
INSERT INTO `detallefactura` VALUES (82, 0, '2', 'Jabon de Cuaba', 23, 3);
INSERT INTO `detallefactura` VALUES (82, 1, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (84, 0, '7467634947812', 'Libro de Bachiller 4to', 2, 2);
INSERT INTO `detallefactura` VALUES (85, 0, '07560818250057', 'Articulo papel', 15, 3);
INSERT INTO `detallefactura` VALUES (86, 0, '07560818250057', 'Articulo papel', 15, 2);
INSERT INTO `detallefactura` VALUES (87, 0, '07560818250057', 'Articulo papel', 15, 3);
INSERT INTO `detallefactura` VALUES (88, 0, '07560818250057', 'Articulo papel', 15, 2);
INSERT INTO `detallefactura` VALUES (88, 1, '2', 'Jabon de Cuaba', 23, 7);
INSERT INTO `detallefactura` VALUES (88, 2, '000', 'PRODUCTO CON COSTO', 35.22, 7);
INSERT INTO `detallefactura` VALUES (89, 0, '048231294461', 'DVD-R de 4.7 GB 8X', 16, 23.22);
INSERT INTO `detallefactura` VALUES (90, 0, '4', 'LOLA BIEN', 513, 5);
INSERT INTO `detallefactura` VALUES (91, 0, '4', 'LOLA BIEN', 513, 5);
INSERT INTO `detallefactura` VALUES (92, 0, '07560818250057', 'Articulo papel', 15, 3);
INSERT INTO `detallefactura` VALUES (93, 0, '2', 'Jabon de Cuaba', 23, 4);
INSERT INTO `detallefactura` VALUES (93, 1, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 4);
INSERT INTO `detallefactura` VALUES (93, 2, '1', 'COCA COLA X 350 ML', 41.48, 4);
INSERT INTO `detallefactura` VALUES (93, 3, '4', 'LOLA BIEN', 513, 5);
INSERT INTO `detallefactura` VALUES (94, 0, '2', 'Jabon de Cuaba', 23, 4);
INSERT INTO `detallefactura` VALUES (94, 1, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 4);
INSERT INTO `detallefactura` VALUES (94, 2, '1', 'COCA COLA X 350 ML', 41.48, 4);
INSERT INTO `detallefactura` VALUES (94, 3, '4', 'LOLA BIEN', 513, 5);
INSERT INTO `detallefactura` VALUES (95, 0, '1', 'COCA COLA X 350 ML', 41.48, 5);
INSERT INTO `detallefactura` VALUES (95, 1, '2', 'Jabon de Cuaba', 23, 3);
INSERT INTO `detallefactura` VALUES (95, 2, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (95, 3, 'xxx', 'xxx', 59.34, 4);
INSERT INTO `detallefactura` VALUES (96, 0, '1', 'COCA COLA X 350 ML', 41.48, 2);
INSERT INTO `detallefactura` VALUES (96, 1, '2', 'Jabon de Cuaba', 23, 3);
INSERT INTO `detallefactura` VALUES (96, 2, '4', 'LOLA BIEN', 513, 5);
INSERT INTO `detallefactura` VALUES (97, 0, '1', 'COCA COLA X 350 ML', 41.48, 5);
INSERT INTO `detallefactura` VALUES (97, 1, '2', 'Jabon de Cuaba', 23, 6);

-- --------------------------------------------------------

-- 
-- Table structure for table `factura`
-- 

CREATE TABLE `factura` (
  `idFactura` int(11) NOT NULL,
  `idCliente` varchar(14) NOT NULL,
  `fecha` date NOT NULL,
  `idUsuario` varchar(45) default NULL,
  PRIMARY KEY  (`idFactura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `factura`
-- 

INSERT INTO `factura` VALUES (2, '012-0089344-1', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (3, '012-0089344-1', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (4, '012-0089344-1', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (5, '012-0089344-2', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (6, '012-0089344-2', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (7, '012-0089344-2', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (8, '012-0089344-1', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (9, '012-0089344-1', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (10, '012-0089344-2', '2013-10-22', NULL);
INSERT INTO `factura` VALUES (11, '012-0089344-1', '2013-10-24', NULL);
INSERT INTO `factura` VALUES (12, '012-0089344-1', '2013-10-24', NULL);
INSERT INTO `factura` VALUES (13, '012-0089344-2', '2013-10-24', NULL);
INSERT INTO `factura` VALUES (14, '012-0089344-3', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (15, '012-0089344-3', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (16, '012-0089344-2', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (17, '012-0089344-1', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (18, '012-0089344-2', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (19, '012-0046244-8', '2013-10-25', NULL);
INSERT INTO `factura` VALUES (20, '012-0046244-8', '2013-10-26', NULL);
INSERT INTO `factura` VALUES (21, '012-0046244-8', '2013-11-01', NULL);
INSERT INTO `factura` VALUES (22, '012-0089344-3', '2013-11-01', NULL);
INSERT INTO `factura` VALUES (23, '012-0068755-5', '2013-11-01', NULL);
INSERT INTO `factura` VALUES (24, '011-0048599-3', '2013-11-03', NULL);
INSERT INTO `factura` VALUES (25, '012-0089344-2', '2013-11-04', NULL);
INSERT INTO `factura` VALUES (26, '012-0097346-8', '2013-11-04', NULL);
INSERT INTO `factura` VALUES (27, '012-0089344-1', '2013-11-05', NULL);
INSERT INTO `factura` VALUES (28, '0', '2013-11-05', NULL);
INSERT INTO `factura` VALUES (29, '0', '2013-11-05', NULL);
INSERT INTO `factura` VALUES (30, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (31, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (32, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (33, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (34, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (35, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (36, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (37, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (38, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (39, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (40, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (41, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (42, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (43, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (44, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (45, '0', '2013-11-06', NULL);
INSERT INTO `factura` VALUES (46, '0', '2013-11-09', NULL);
INSERT INTO `factura` VALUES (47, '012-0089344-1', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (48, '012-0046244-8', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (49, '011-0048599-3', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (50, '012-0089344-1', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (51, '012-0089344-2', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (52, '011-0048599-3', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (53, '012-0068755-5', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (54, '011-0048599-3', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (55, '012-0068755-5', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (56, '011-0048599-3', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (57, '012-0089344-2', '2013-11-10', NULL);
INSERT INTO `factura` VALUES (58, '012-0046244-8', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (59, '012-0068755-5', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (60, '011-0048599-3', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (61, '011-0048599-3', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (62, '012-0046244-8', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (63, '012-0068755-5', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (64, '012-0046244-8', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (65, '012-0068755-5', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (66, '012-0068755-5', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (67, '012-0089344-1', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (68, '012-0089344-1', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (69, '012-0089344-2', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (70, '012-0089344-3', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (71, '012-0089344-1', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (72, '012-0089344-1', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (73, '012-0068755-5', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (74, '0', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (75, '0', '2013-11-11', NULL);
INSERT INTO `factura` VALUES (76, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (77, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (78, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (79, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (80, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (81, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (82, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (83, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (84, '0', '2013-11-12', NULL);
INSERT INTO `factura` VALUES (85, '0', '2013-11-14', NULL);
INSERT INTO `factura` VALUES (86, '0', '2013-11-14', NULL);
INSERT INTO `factura` VALUES (87, '0', '2013-11-14', NULL);
INSERT INTO `factura` VALUES (88, '0', '2013-11-16', NULL);
INSERT INTO `factura` VALUES (89, '012-0097346-8', '2013-11-16', NULL);
INSERT INTO `factura` VALUES (90, '012-0089344-1', '2013-11-16', NULL);
INSERT INTO `factura` VALUES (91, '012-0089344-1', '2013-11-16', NULL);
INSERT INTO `factura` VALUES (92, 'Urania Montas', '2013-12-09', NULL);
INSERT INTO `factura` VALUES (93, '012-0094902-0', '2013-12-10', NULL);
INSERT INTO `factura` VALUES (94, '012-0094902-0', '2013-12-10', NULL);
INSERT INTO `factura` VALUES (95, '0', '2013-12-16', 'jhironsel');
INSERT INTO `factura` VALUES (96, '0', '2013-12-16', 'jhironsel');
INSERT INTO `factura` VALUES (97, '012-0668966-2', '2013-12-28', 'jhironsel');

-- --------------------------------------------------------

-- 
-- Table structure for table `productos`
-- 

CREATE TABLE `productos` (
  `idProducto` varchar(24) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `idIVA` int(11) NOT NULL,
  `notas` text,
  `costo` float NOT NULL,
  `entrada` float NOT NULL,
  `estado` varchar(8) NOT NULL,
  `fechaVencimiento` date default NULL,
  PRIMARY KEY  (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `productos`
-- 

INSERT INTO `productos` VALUES ('0', 'PRODUCTO CON COSTO', 35.22, 1, 'PRODUCTO CON COSTO22', 33.32, 22, 'INACTIVO', '2013-12-19');
INSERT INTO `productos` VALUES ('048231294461', 'DVD-R de 4.7 GB 8X', 16, 1, 'DVD de Pruebas', 12, 25.78, 'INACTIVO', '2014-05-23');
INSERT INTO `productos` VALUES ('07560818250057', 'Articulo papel', 15, 1, 'localizado en tal parte', 10, 37, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('1', 'COCA COLA X 350 ML', 41.48, 1, 'Sabe mejor bien Helada', 38, -6, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('10', 'Queso Amarillo', 60, 2, 'Trabajo Casi Terminado', 49.55, 5344, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('11', 'PROBANDO CON ID ALFANUMERICOS', 233, 0, '	FFFF', 23, 3, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('12', 'COMO VA ESTO', 36, 1, 'PROBANDOLO TODAVIA', 34, 221.97, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('13', 'ENSALADA LIB. 1', 34, 1, 'PREPARAR', 23, 324, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('14', 'AZUCAR PRIETA LIBRA', 23.5, 2, 'AZUCAR POR UN TUVO modificado para probar', 19.44, 150.2, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('2', 'Jabon de Cuaba', 23, 2, 'PRODUCTO PARA EVALUAR SISTEMA....', 20, -38.8, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 1, 'PRODUCTO DE PRUEBAS', 145, 16, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('4', 'LOLA BIEN', 513, 1, 'ALGO DE NOTAS PARA PROBAR	2', 450.3, 6, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('5', 'XXX', 23.45, 1, 'XXX', 21.56, 25, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('6', 'PRODUCTO', 43, 2, 'ESTO ES PRUEBAS', 36, 14.2, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('7', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 0, 'ESTO SE IVA A QUEDAR EN BLANCO', 32.45, 30, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('7467634947812', 'Libro de Bachiller 4to', 2, 0, 'Mostrador de Alante', 1, 9, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('7501015213827', 'Marcador de Agua Verde', 15, 1, 'Marcador verde de NIN', 8, 69, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('7501056330255', 'ponds cont. neto 50g', 100, 1, 'localizado en el mostrador del negocio.\n\n\n', 85, 18, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('8', 'Cuando terminar', 33.4, 1, 'dddnnn', 23.3, 344.33, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('9', 'HARINA DEL NEGRITO', 43.44, 0, 'PREPARAR HARINA', 32, 324, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('9844', 'PRODUCTO PARA ESTADO', 32.43, 2, 'SALUDOS CON QUESO Y PAPA CARAJOS', 32.42, -1, 'ACTIVO', NULL);
INSERT INTO `productos` VALUES ('Carbon22', 'Carbon de bueno Lib', 64, 1, 'Todo va bien con netbeans 7.3.1', 23.44, 100, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('jfji923', 'Producto Boton modificados', 42.3, 1, 'Olvide nota', 23.33, 34, 'INACTIVO', NULL);
INSERT INTO `productos` VALUES ('xxx', 'xxx', 59.34, 0, 'ssss', 23, 41, 'ACTIVO', '2013-12-20');

-- --------------------------------------------------------

-- 
-- Table structure for table `provedor`
-- 

CREATE TABLE `provedor` (
  `idProvedor` varchar(13) NOT NULL,
  `nombres` varchar(25) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  PRIMARY KEY  (`idProvedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `provedor`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `usuarios`
-- 

CREATE TABLE `usuarios` (
  `idUsuario` varchar(13) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `clave` varchar(10) NOT NULL,
  `idPerfil` int(11) NOT NULL,
  `foto` varchar(70) default NULL,
  PRIMARY KEY  (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `usuarios`
-- 

INSERT INTO `usuarios` VALUES ('Alberto', 'ALBERTO', 'CONTRERAS', '11', 2, NULL);
INSERT INTO `usuarios` VALUES ('Eduardo', 'PALILLO EDUARDO', 'PEREZ ALMONTE', '123', 2, NULL);
INSERT INTO `usuarios` VALUES ('Galfoncel', 'GALF', 'LORENZO PEREZ', '4321', 1, NULL);
INSERT INTO `usuarios` VALUES ('Jhironsel', 'Jhironsel', 'Diaz Almonte', '123', 1, '');
INSERT INTO `usuarios` VALUES ('jhironsel1', 'Jhironsel', 'Diaz', 'zxcv', 2, '');
INSERT INTO `usuarios` VALUES ('Maria', 'Mary Lucia', 'Perez Perez 1', '1234', 1, NULL);
INSERT INTO `usuarios` VALUES ('Monica', 'MONICA', 'VARGAS MORDAN', '123', 2, NULL);
INSERT INTO `usuarios` VALUES ('Sophia', 'Sophia', 'Diaz Vargas', '123', 1, NULL);
INSERT INTO `usuarios` VALUES ('yiro', 'yironsel', 'Perez', 'zxc', 2, 'Sofia.jpg');
