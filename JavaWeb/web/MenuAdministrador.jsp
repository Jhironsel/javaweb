<%-- 
    Document   : MenuAdministrador
    Created on : 31-dic-2014, 18:53:32
    Author     : Jhironsel
--%>

<%@page autoFlush="true" buffer="1094kb"%>
<%@page import="clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="Encabezado.jsp"></jsp:include>
            <title>Sistema de Facturacion [Menu Administrador]</title>
        </head>
        <body>
        <%
            //Para trabajar con la sesion del usuario actual
            HttpSession sesion = request.getSession();
            Usuario miUsuario = (Usuario) sesion.getAttribute("usuario");
            if (miUsuario == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            if (miUsuario.getIdPerfil() != 1) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>

        <h1>Menu Principal</h1>
        <h2>Bienvenido: <%=miUsuario.getNombres() + " " + miUsuario.getApellidos()%></h2>
        <%  
            String foto = "";
            foto = miUsuario.getFoto();
            
            if(foto == null) foto="";
            
            if (foto.equals("")) {
        %>
        <img src="images/user.png" width="128" height="128"/>
        <br>
        <%
            } else {
        %>
        <img src=<%="images/" + miUsuario.getFoto()%> width="128" height="128"/>        
        <br>
        <%
            }
        %>
        <br>
        <a href="Clientes.jsp">Clientes</a><br>
        <a href="Productos.jsp">Productos</a><br>
        <a href="Usuarios.jsp">Usuarios</a><br>
        <a href="NuevaFactura.jsp">Nueva Factura</a><br>
        <a href="ReporteFacturas.jsp">Reporte de Factura</a><br>
        <a href="index.jsp">Salir</a>

    </body>
</html>
