<%-- 
    Document   : Usuarios
    Created on : 01-ene-2015, 18:28:21
    Author     : Jhironsel
--%>

<%@page import="javax.swing.text.Document"%>
<%@page import="clases.Usuario"%>
<%@page import="clases.Datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="Encabezado.jsp"></jsp:include>
            <title>Sistema de Facturacion [Usuarios]</title>

            <script>
                $(document).ready(function () {
                    $("#consultar").click(function () {
                        return validaridUsuario();
                    });

                    $("#nuevo").click(function () {
                        return validarTodo();
                    });

                    $("#modificar").click(function () {
                        return validarTodo();
                    });

                    $("#borrar").click(function () {
                        if (validaridUsuario()) {
                            $('<div></div>').html("Esta Seguro de Borrar el Usuario?").dialog(
                                    {title: "Confirmacion", modal: true, buttons: [
                                            {
                                                text: "Si",
                                                click: function () {
                                                    $(this).dialog("close");
                                                    $.post("EliminarUsuario", {idUsuario: $("#idUsuario").val()},
                                                    function (data) {
                                                        $("#idUsuario").val("");
                                                        $("#nombres").val("");
                                                        $("#apellidos").val("");
                                                        $("#clave").val("");
                                                        $("#confirmacion").val("");
                                                        $("#perfil").val("0");
                                                    });
                                                }
                                            },
                                            {
                                                text: "No",
                                                click: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        ]
                                    }
                            );
                        }
                        return false;
                    });
                });


                function validarTodo() {
                    if (validaridUsuario()) {
                        if (validarNombres()) {
                            if (validarApellidos()) {
                                if (validarClave()) {
                                    if (validarConfirmacion()) {
                                        if (validarClaveConfirmacion())
                                            return validarPerfil();
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }

                function validaridUsuario() {
                    if ($("#idUsuario").val() === "") {
                        $("<div></div>").html("Debe Ingresar un ID de Usuario").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarNombres() {
                    if ($("#nombres").val() === "") {
                        $("<div></div>").html("Debe Ingresar un Nombre(s) de Usuario").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarApellidos() {
                    if ($("#apellidos").val() === "") {
                        $("<div></div>").html("Debe Ingresar un Apellidos de Usuario").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarClave() {
                    if ($("#clave").val() === "") {
                        $("<div></div>").html("Debe Ingresar clave de Usuario").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarConfirmacion() {
                    if ($("#confirmacion").val() === "") {
                        $("<div></div>").html("Debe Ingresar clave de confirmacion de Usuario").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarClaveConfirmacion() {
                    if ($("#confirmacion").val() !== $("#clave").val()) {
                        $("<div></div>").html("Clave y Confirmacion no Coinciden").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                
                function validarPerfil() {
                    if ($("#perfil").val() === "0") {
                        $("<div></div>").html("Debe Seleccionar el Perfil").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }                
            </script>
        </head>
        
        <body>
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado == null) {
        %>
                <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }

            if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
                <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>
        
        <%            
            //Variable que muestra los mensaje del sistema
            String mensaje = "";

            //Identificamos el boton que el usuario presiono
            boolean consultar = false;
            boolean nuevo = false;
            boolean modificar = false;
            boolean limpiar = false;
            boolean listar = false;

            if (request.getParameter("consultar") != null) {
                consultar = true;
            }
            if (request.getParameter("nuevo") != null) {
                nuevo = true;
            }
            if (request.getParameter("modificar") != null) {
                modificar = true;
            }
            if (request.getParameter("limpiar") != null) {
                limpiar = true;
            }
            if (request.getParameter("listar") != null) {
                listar = true;
            }
            //Obtenemos el valor como llamado el formulario
            String idUsuario = "";
            String nombres = "";
            String apellidos = "";
            String clave = "";
            String confirmacion = "";
            String perfil = "";
            String foto = "";

            if (request.getParameter("idUsuario") != null) {
                idUsuario = request.getParameter("idUsuario");
            }
            if (request.getParameter("nombres") != null) {
                nombres = request.getParameter("nombres");
            }
            if (request.getParameter("apellidos") != null) {
                apellidos = request.getParameter("apellidos");
            }
            if (request.getParameter("clave") != null) {
                clave = request.getParameter("clave");
            }
            if (request.getParameter("confirmacion") != null) {
                confirmacion = request.getParameter("confirmacion");
            }
            if (request.getParameter("perfil") != null) {
                perfil = request.getParameter("perfil");
            }
            if (request.getParameter("foto") != null) {
                foto = request.getParameter("foto");
            }

            //Si presionan el boton consultar
            if (consultar) {
                Datos misDatos = new Datos();
                Usuario miUsuario = misDatos.getUsuario(idUsuario);

                if (miUsuario == null) {
                    nombres = "";
                    apellidos = "";
                    clave = "";
                    confirmacion = "";
                    perfil = "";
                    foto = "";
                    mensaje = "Usuario No Existe";
                } else {
                    idUsuario = miUsuario.getIdUsuario();
                    nombres = miUsuario.getNombres();
                    apellidos = miUsuario.getApellidos();
                    clave = miUsuario.getClave();
                    confirmacion = miUsuario.getClave();
                    perfil = "" + miUsuario.getIdPerfil();
                    foto = miUsuario.getFoto();
                    mensaje = "Usuario Consultado...";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Limpiar
            if (limpiar) {
                idUsuario = "";
                nombres = "";
                apellidos = "";
                clave = "";
                confirmacion = "";
                perfil = "";
                foto = "";
                mensaje = "";
            }

            //Si presionan el boton Nuevo
            if (nuevo) {
                Datos misDatos = new Datos();
                Usuario miUsuario = misDatos.getUsuario(idUsuario);
                if (miUsuario != null) {
                    mensaje = "Usuario Ya Existe";
                } else {
                    miUsuario = new Usuario(
                            idUsuario,
                            nombres,
                            apellidos,
                            clave,
                            new Integer(perfil),
                            foto);
                    misDatos.newUsuario(miUsuario);
                    mensaje = "Usuario Ingresado";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Modificar
            if (modificar) {
                Datos misDatos = new Datos();
                Usuario miUsuario = misDatos.getUsuario(idUsuario);

                if (miUsuario == null) {
                    mensaje = "Usuario no Existe";
                } else {
                    miUsuario = new Usuario(
                            idUsuario,
                            nombres,
                            apellidos,
                            clave,
                            new Integer(perfil),
                            foto);
                    misDatos.updateUsuario(miUsuario);
                    mensaje = "Usuario Modificado";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Listar
            if (listar) {
        %>
                <script>
                    document.location.href = "ListadoUsuarios.jsp";
                </script>        
        <%
            }
        %>

        <h1>Usuarios</h1>
        <form name="usuarios" id="usuarios" action="Usuarios.jsp" method="POST">
            <table border="0">                
                <tbody>
                    <tr>
                        <td>Id Usuario:</td>
                        <td><input type="text" name="idUsuario" id="idUsuario" value="<%=idUsuario%>" size="13" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Nombres:</td>
                        <td><input type="text" name="nombres" id="nombres" value="<%=nombres%>" size="30" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Apellidos:</td>
                        <td><input type="text" name="apellidos" id="apellidos" value="<%=apellidos%>" size="30" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Clave:</td>
                        <td><input type="password" name="clave" id="clave" value="<%=clave%>" size="10" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Confirmacion de Clave:</td>
                        <td><input type="password" name="confirmacion" id="confirmacion" value="<%=confirmacion%>" size="10" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Perfil:</td>
                        <td><select name="perfil" id="perfil">
                                <option value="0" <%=(perfil.equals("") ? "selected" : "")%>>Seleccione perfil...</option>
                                <option value="1" <%=(perfil.equals("1") ? "selected" : "")%>>Administrador</option>
                                <option value="2" <%=(perfil.equals("2") ? "selected" : "")%>>Empleado</option>                                
                            </select> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Foto:</td>
                        <td><input type="file" name="foto" id="foto" value="<%=foto%>" />
                            <%
                                if (!foto.equals("")) {
                            %>
                            <br>
                            <img src="<%="images/" + foto%>" width="150" height="150"/>
                            <%
                                } else {
                            %>
                            <br>
                            <img src="images/user.png" width="150" height="150"/>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" ><font color="red">* Campos Obligatorio</font></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <jsp:include page="Botones.jsp"></jsp:include>
       </form>
        <br>
        <h1><%=mensaje%></h1>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>        
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>


    </body>
</html>