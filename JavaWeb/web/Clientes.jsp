<%-- 
    Document   : Clientes
    Created on : 01-ene-2015, 18:28:21
    Author     : Jhironsel
--%>

<%@page import="java.util.Date"%>
<%@page import="clases.Utilidades"%>
<%@page import="clases.Cliente"%>
<%@page import="javax.swing.text.Document"%>
<%@page import="clases.Usuario"%>
<%@page import="clases.Datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="Encabezado.jsp"></jsp:include>
            <title>Sistema de Facturacion [Clientes]</title>
            <script>
                $(document).ready(function () {
                    $("#fechaNacimiento").datepicker({monthNamesShort: ["Enero",
                            "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                            "Diciembre"], dateFormat: "dd.MM.yyyy", changeYear: true,
                        changeMonth: true});

                    $("#consultar").click(function () {
                        return validaridCliente();
                    });

                    $("#nuevo").click(function () {
                        return validarTodo();
                    });

                    $("#modificar").click(function () {
                        return validarTodo();
                    });

                    $("#borrar").click(function () {
                        if (validaridCliente()) {
                            $('<div></div>').html("Esta Seguro de Borrar el Cliente?").dialog(
                                    {title: "Confirmacion", modal: true, buttons: [
                                            {
                                                text: "Si",
                                                click: function () {
                                                    $(this).dialog("close");
                                                    $.post("EliminarCliente", {idCliente: $("#idCliente").val()},
                                                            function (data) {
                                                                $("#idCliente").val("");
                                                                $("#idTipo").val("0");
                                                                $("#nombres").val("");
                                                                $("#apellidos").val("");
                                                                $("#direccion").val("");
                                                                $("#telefono").val("");
                                                                $("#idCiudad").val("0");
                                                                $("#fechaIngreso").val("");
                                                                $("#fechaNacimiento").val("");
                                                                $("#credito").val("");
                                                            });
                                                }
                                            },
                                            {
                                                text: "No",
                                                click: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        ]
                                    }
                            );
                        }
                        return false;
                    });
                });

                function validarTodo() {
                    if (validaridCliente()) {
                        if (validaridTipo()) {
                            if (validarNombres()) {
                                return validarApellidos();
                            }
                        }
                    }
                    return false;
                }

                function validaridCliente() {
                    if ($("#idCliente").val() === "") {
                        $("<div></div>").html("Debe Ingresar un ID de Cliente").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validaridTipo() {
                    if ($("#idTipo").val() === "0") {
                        $("<div></div>").html("Debe Seleccionar un Tipo de Identificacion de Cliente!").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarNombres() {
                    if ($("#nombres").val() === "") {
                        $("<div></div>").html("Debe Ingresar un Nombre(s) de Cliente").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
                function validarApellidos() {
                    if ($("#apellidos").val() === "") {
                        $("<div></div>").html("Debe Ingresar un Apellidos de Cliente").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

            </script>
        </head>

        <body>
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>

        <%
            //Variable que muestra los mensaje del sistema
            String mensaje = "";

            //Identificamos el boton que el usuario presiono
            boolean consultar = false;
            boolean nuevo = false;
            boolean modificar = false;
            boolean limpiar = false;
            boolean listar = false;

            if (request.getParameter("consultar") != null) {
                consultar = true;
            }
            if (request.getParameter("nuevo") != null) {
                nuevo = true;
            }
            if (request.getParameter("modificar") != null) {
                modificar = true;
            }
            if (request.getParameter("limpiar") != null) {
                limpiar = true;
            }
            if (request.getParameter("listar") != null) {
                listar = true;
            }
            //Obtenemos el valor como llamado el formulario
            String idCliente = "";
            String idTipo = "";
            String nombres = "";
            String apellidos = "";
            String direccion = "";
            String telefono = "";
            String idCiudad = "";
            String fechaNacimiento = "";
            String fechaIngreso = "";
            String credito = "";
            String deudaActual = "";

            if (request.getParameter("idCliente") != null) {
                idCliente = request.getParameter("idCliente");
            }
            if (request.getParameter("idTipo") != null) {
                idTipo = request.getParameter("idTipo");
            }
            if (request.getParameter("nombres") != null) {
                nombres = request.getParameter("nombres");
            }
            if (request.getParameter("apellidos") != null) {
                apellidos = request.getParameter("apellidos");
            }
            if (request.getParameter("direccion") != null) {
                direccion = request.getParameter("direccion");
            }
            if (request.getParameter("telefono") != null) {
                telefono = request.getParameter("telefono");
            }
            if (request.getParameter("idCiudad") != null) {
                idCiudad = request.getParameter("idCiudad");
            }
            if (request.getParameter("fechaNacimiento") != null) {
                fechaNacimiento = request.getParameter("fechaNacimiento");
            }
            if (request.getParameter("fechaIngreso") != null) {
                fechaIngreso = request.getParameter("fechaIngreso");
            }
            if (request.getParameter("credito") != null) {
                credito = request.getParameter("credito");
            }

            //Si presionan el boton consultar
            if (consultar) {
                Datos misDatos = new Datos();
                Cliente miCliente = misDatos.getCliente(idCliente);

                if (miCliente == null) {
                    idTipo = "";
                    nombres = "";
                    apellidos = "";
                    direccion = "";
                    telefono = "";
                    idCiudad = "";
                    fechaNacimiento = "";
                    fechaIngreso = "";
                    credito = "";
                    deudaActual = "";
                    mensaje = "Cliente No Existe!";
                } else {
                    idCliente = miCliente.getIdCliente();
                    idTipo = "" + miCliente.getIdTipo();
                    nombres = miCliente.getNombres();
                    apellidos = miCliente.getApellidos();
                    direccion = miCliente.getDireccion();
                    telefono = miCliente.getTelefono();
                    idCiudad = "" + miCliente.getIdCiudad();
                    fechaNacimiento = Utilidades.formatDate(miCliente.getFechaNacimiento());
                    fechaIngreso = Utilidades.formatDate(miCliente.getFechaIngreso());
                    credito = "" + miCliente.getCredito();
                    mensaje = "Cliente Consultado...";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Limpiar
            if (limpiar) {
                idCliente = "";
                idTipo = "";
                nombres = "";
                apellidos = "";
                direccion = "";
                telefono = "";
                idCiudad = "";
                fechaNacimiento = "";
                fechaIngreso = "";
                credito = "";
                deudaActual = "";
                mensaje = "";
            }

            //Si presionan el boton Nuevo
            if (nuevo) {
                Datos misDatos = new Datos();
                Cliente miCliente = misDatos.getCliente(idCliente);
                if (miCliente != null) {
                    mensaje = "Cliente Ya Existe";
                } else {
                    miCliente = new Cliente(
                            idCliente,
                            new Integer(idTipo),
                            nombres,
                            apellidos,
                            direccion,
                            telefono,
                            new Integer(idCiudad),
                            Utilidades.stringToDate(fechaNacimiento),
                            new Date(),
                            new Float(credito),
                            new Float("0.0"));
                    misDatos.newCliente(miCliente);
                    mensaje = "Cliente Ingresado";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Modificar
            if (modificar) {
                Datos misDatos = new Datos();
                Cliente miCliente = misDatos.getCliente(idCliente);
                if (miCliente == null) {
                    mensaje = "Cliente no Existe";
                } else {
                    miCliente = new Cliente(
                            idCliente,
                            new Integer(idTipo),
                            nombres,
                            apellidos,
                            direccion,
                            telefono,
                            new Integer(idCiudad),
                            Utilidades.stringToDate(fechaNacimiento),
                            Utilidades.stringToDate(fechaIngreso),
                            new Float(credito),
                            miCliente.getDeudaActual());
                    misDatos.updateCliente(miCliente);
                    mensaje = "Cliente Modificado";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Listar
            if (listar) {
        %>
        <script>
            document.location.href = "ListadoClientes.jsp";
        </script>        
        <%
            }
        %>

        <h1>Clientes</h1>
        <form name="clientes" id="clientes" action="Clientes.jsp" method="POST">
            <table border="0">                
                <tbody>
                    <tr>
                        <td>Id Cliente:</td>
                        <td><input type="text" name="idCliente" id="idCliente" value="<%=idCliente%>" size="13"/><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Tipo de Id:</td>
                        <td><select name="idTipo" id="idTipo">
                                <option value="0" <%=(idTipo.equals("") ? "selected" : "")%>>Seleccione Tipo de Id...</option>
                                <option value="1" <%=(idTipo.equals("1") ? "selected" : "")%>>Cedula</option>
                                <option value="2" <%=(idTipo.equals("2") ? "selected" : "")%>>Pasaporte</option>
                                <option value="3" <%=(idTipo.equals("3") ? "selected" : "")%>>Nombre de Institucion</option>
                            </select> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Nombres:</td>
                        <td><input type="text" name="nombres" id="nombres" value="<%=nombres%>" size="30" /> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Apellidos:</td>
                        <td><input type="text" name="apellidos" id="apellidos" value="<%=apellidos%>" size="30" /> <font color="red">*</font> </td>
                    </tr>                    
                    <tr>
                        <td>Direccion:</td>
                        <td><input type="text" name="direccion" id="direccion" value="<%=direccion%>" size="30" /></td>
                    </tr>                    
                    <tr>
                        <td>Telefono:</td>
                        <td><input type="text" name="telefono" id="telefono" value="<%=telefono%>" size="14" /></td>
                    </tr>                    
                    <tr>
                        <td>Ciudad:</td>
                        <td><select name="idCiudad" id="idCiudad">
                                <option value="0" <%=(idCiudad.equals("") ? "selected" : "")%>>Seleccione perfil...</option>
                                <option value="1" <%=(idCiudad.equals("1") ? "selected" : "")%>>San Juan</option>
                                <option value="2" <%=(idCiudad.equals("2") ? "selected" : "")%>>Juan Herrera</option>                                
                                <option value="3" <%=(idCiudad.equals("3") ? "selected" : "")%>>Las Matas</option>                                
                                <option value="4" <%=(idCiudad.equals("4") ? "selected" : "")%>>El Cercado</option>                                
                            </select></td>
                    </tr>
                    <tr>
                        <td>Fecha de Nacimiento:</td>
                        <td><input type="text" name="fechaNacimiento" id="fechaNacimiento" value="<%=fechaNacimiento%>" size="10" /></td>
                    </tr>
                    <tr>
                        <td>Fecha de Ingreso:</td>
                        <td><input type="text" name="fechaIngreso" id="fechaIngreso" value="<%=fechaIngreso%>" size="10" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td>Credito:</td>
                        <td><input type="text" name="credito" id="credito" value="<%=credito%>" size="10" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" ><font color="red">* Campos Obligatorio</font></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <jsp:include page="Botones.jsp"></jsp:include>
            </form>
            <br>
            <h1><%=mensaje%></h1>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>        
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>
    </body>
</html>