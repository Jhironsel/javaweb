<%@page import="clases.Usuario"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="clases.Datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Facturacion [Listado de Cliente]</title>
        <jsp:include page="Encabezado.jsp"></jsp:include>
    </head>
    <body>        
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
            <jsp:forward page="index.jsp"></jsp:forward>       
        <%
            }
            if (miUsuarioLogueado == null) {
        %>
            <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>

        <h1>Listado de Cliente</h1>
        <table border="1" cellpadding="20">
            <thead>
                <tr>
                    <th>Id Cliente</th>
                    <th>id Tipo</th>
                    <th>Nombre Completo</th>
                    <th>Direccion</th>
                    <th>Telefono</th>
                    <th>Ciudad</th>
                    <th>Fecha de Nacimiento</th>
                    <th>Fecha de Ingreso</th>
                    <th>Credito</th>
                    <th>Deuda Actual</th>
                </tr>
            </thead>
            <tbody>
                <%
                    //Para llenar la Tabla
                    Datos misDatos = new Datos();
                    ResultSet rs = misDatos.getClientes();
                    //Para recorrer la base de Datos y llenar la Tabla del Aplicativo
                    while (rs.next()) {
                %>
                <tr>
                    <td><%=rs.getString("idCliente")%></td>
                    <td><%=(rs.getString("idTipo").equals("1") ? "Cedula":
                            rs.getString("idTipo").equals("2") ? "Pasaporte":
                            rs.getString("idTipo").equals("3") ? "Nombre de Institucion":
                            "Sin Definir")%>
                    </td>
                    <td><%=rs.getString("nombres") +" "+ rs.getString("apellidos")%></td>
                    <td><%=rs.getString("direccion")%></td>
                    <td><%=rs.getString("telefono")%></td>
                    <td><%=(rs.getString("idCiudad").equals("1") ? "San Juan":
                            rs.getString("idCiudad").equals("2") ? "Juan Herrera":
                            rs.getString("idCiudad").equals("3") ? "Las Matas":
                            rs.getString("idCiudad").equals("4") ? "El Cercado":
                            "Sin Definir")%></td>
                    <td><%=rs.getString("fechaNacimiento")%></td>
                    <td><%=rs.getString("fechaIngreso")%></td>
                    <td align="right"><%=rs.getString("credito")%></td>
                    <td align="right"><%=rs.getString("deudaActual")%></td>                    
                </tr>
                <%
                    }                    
                %>
            </tbody>
            <thead>
                <tbody>               
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>*==>   Total:</th>
                    <th><%=misDatos.deuda()%></th>
                </tbody>
                <%
                    misDatos.cerrarConexion();
                %>
            </thead>
        </table>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>        
    </body>
</html>
