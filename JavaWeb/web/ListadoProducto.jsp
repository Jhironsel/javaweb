<%-- 
    Document   : ListadoUsuario
    Created on : 01/16/2015, 10:56:15 AM
    Author     : jhironsel
--%>

<%@page import="clases.Usuario"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="clases.Datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Facturacion [Listado de Productos]</title>
        <jsp:include page="Encabezado.jsp"></jsp:include>
        </head>
        <body>        
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            if (miUsuarioLogueado == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>

        <h1>Listado de Producto</h1>
        
        <table border="1">
            <thead>
                <tr>
                    <th>ID Producto</th>
                    <th>Descripcion</th>
                    <th>Precio</th>
                    <th>Impuesto</th>
                    <th>Notas</th>
                    <th>Costo</th>
                    <th>Existencia</th>
                    <th>Estado</th>
                    <th>Fecha de Vencimiento</th>                    
                    <th>Foto</th>                    
                </tr>
            </thead>
            <tbody>
                <%
                    //Para llenar la Tabla
                    Datos misDatos = new Datos();
                    ResultSet rs = misDatos.getProducto();
                    //Para recorrer la base de Datos y llenar la Tabla del Aplicativo
                    while (rs.next()) {
                %>
                <tr>
                    <td><%=rs.getString("idProducto")%></td>
                    <td><%=rs.getString("descripcion")%></td>
                    <td><%=rs.getString("precio")%></td>
                    <td><%=rs.getString("idIVA")%></td>
                    <td><%=rs.getString("notas")%></td>
                    <td><%=rs.getString("costo")%></td>
                    <td><%=rs.getString("entrada")%></td>
                    <td><%=rs.getString("estado")%></td>
                    <td><%=rs.getString("fechaVencimiento")%></td>
                    <%
                        if (!rs.getString("foto").equals("")) {
                    %>
                    <td><img src= "<%="productos/" + rs.getString("foto")%>"  width="96" height="96"/></td>
                    <%
                        } else {
                    %>
                    <td><img src="images/lupa.png" width="96" height="96"/></td>
                    <%
                        }                    
                    %>
                </tr>
                <%
                    }
                    misDatos.cerrarConexion();
                %>
            </tbody>
        </table>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>        
    </body>
</html>
