<%-- 
    Document   : Usuarios
    Created on : 01-ene-2015, 18:28:21
    Author     : Jhironsel
--%>
<%@page autoFlush="true" buffer="1094kb"%>

<%@page import="clases.Utilidades"%>
<%@page import="clases.Producto"%>
<%@page import="clases.Usuario"%>
<%@page import="clases.Datos"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="Encabezado.jsp"></jsp:include>
            <title>Sistema de Facturacion [Productos]</title>
            <script>
                $(document).ready(function () {
                    $("#fechaVencimiento").datepicker({monthNamesShort: ["Enero",
                            "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                            "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                            "Diciembre"], dateFormat: "dd.MM.yyyy", changeYear: true,
                        changeMonth: true
                    });

                    $("#consultar").click(function () {
                        return validaridProducto();
                    });

                    $("#nuevo").click(function () {
                        return validarTodo();
                    });

                    $("#modificar").click(function () {
                        return validarTodo();
                    });

                    $("#borrar").click(function () {
                        if (validaridProducto()) {
                            $('<div></div>').html("Esta Seguro de Borrar el Usuario?").dialog(
                                    {title: "Confirmacion", modal: true, buttons: [
                                            {
                                                text: "Si",
                                                click: function () {
                                                    $(this).dialog("close");
                                                    $.post("EliminarProducto", {idProducto: $("#idProducto").val()},
                                                    function (data) {
                                                        $("#idProducto").val("");
                                                        $("#descripcion").val("");
                                                        $("#precio").val("");
                                                        $("#idIVA").val("");
                                                        $("#notas").val("");
                                                        $("#costo").val("");
                                                        $("#entrada").val("");
                                                        $("#estado").val("0");
                                                        $("#fechaVencimiento").val("");
                                                        $("#foto").val("");
                                                    });
                                                }
                                            },
                                            {
                                                text: "No",
                                                click: function () {
                                                    $(this).dialog("close");
                                                }
                                            }
                                        ]
                                    }
                            );
                        }
                        return false;
                    });
                });


                function validarTodo() {
                    if (validaridProducto()) {
                        if (validarDescripcion()) {
                            if (validarPrecio()) {
                                if (validarImpuesto()) {
                                    if (validarCosto()) {
                                        if (validarEntrada())
                                            return validarEstado();
                                    }
                                }
                            }
                        }
                    }
                    return false;
                }

                function validaridProducto() {
                    if ($("#idProducto").val() === "") {
                        $("<div></div>").html("Debe Ingresar un ID de Producto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarDescripcion() {
                    if ($("#descripcion").val() === "") {
                        $("<div></div>").html("Debe Ingresar una Descripcion del Producto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarPrecio() {
                    if ($("#precio").val() === "") {
                        $("<div></div>").html("Debe Digitar un precio del Producto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarImpuesto() {
                    if ($("#idIVA").val() === "0") {
                        $("<div></div>").html("Debe Seleccionar un Tipo de Impuesto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarCosto() {
                    if ($("#costo").val() === "") {
                        $("<div></div>").html("Debe Digitar un Costo").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarEntrada() {
                    if ($("#entrada").val() === "") {
                        $("<div></div>").html("Debe Digitar las cantidades de entradas del Producto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarEstado() {
                    if ($("#estado").val() === "0") {
                        $("<div></div>").html("Debe Seleccionar el estado del Producto").
                                dialog({title: "Error de Validacion", modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }
            </script>
        </head>
        <body>
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }else if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
        %>

        <%
            //Variable que muestra los mensaje del sistema
            String mensaje = "";

            //Identificamos el boton que el usuario presiono
            boolean consultar = false;
            boolean nuevo = false;
            boolean modificar = false;
            boolean limpiar = false;
            boolean listar = false;

            if (request.getParameter("consultar") != null) {
                consultar = true;
            }
            if (request.getParameter("nuevo") != null) {
                nuevo = true;
            }
            if (request.getParameter("modificar") != null) {
                modificar = true;
            }
            if (request.getParameter("limpiar") != null) {
                limpiar = true;
            }
            if (request.getParameter("listar") != null) {
                listar = true;
            }
            //Obtenemos el valor como llamado el formulario
            String idProducto = "";
            String descripcion = "";
            String precio = "";
            String idIVA = "";
            String notas = "";
            String costo = "";
            String entrada = "";
            String estado = "";
            String fechaVencimiento = "";
            String foto = "";

            if (request.getParameter("idProducto") != null) {
                idProducto = request.getParameter("idProducto");
            }
            if (request.getParameter("descripcion") != null) {
                descripcion = request.getParameter("descripcion");
            }
            if (request.getParameter("precio") != null) {
                precio = request.getParameter("precio");
            }
            if (request.getParameter("idIVA") != null) {
                idIVA = request.getParameter("idIVA");
            }
            if (request.getParameter("notas") != null) {
                notas = request.getParameter("notas");
            }
            if (request.getParameter("costo") != null) {
                costo = request.getParameter("costo");
            }
            if (request.getParameter("entrada") != null) {
                entrada = request.getParameter("entrada");
            }
            if (request.getParameter("estado") != null) {
                estado = request.getParameter("estado");
            }
            if (request.getParameter("fechaVencimiento") != null) {
                fechaVencimiento = request.getParameter("fechaVencimiento");
            }
            if (request.getParameter("foto") != null) {
                foto = request.getParameter("foto");
            }

            //Si presionan el boton consultar
            if (consultar) {
                Datos misDatos = new Datos();
                Producto miProducto = misDatos.getProducto(idProducto);

                if (miProducto == null) {
                    descripcion = "";
                    precio = "";
                    idIVA = "";
                    notas = "";
                    costo = "";
                    entrada = "";
                    estado = "";
                    fechaVencimiento = "";
                    foto = "";
                    mensaje = "Producto No Existe";
                } else {
                    descripcion = miProducto.getDescripcion();
                    precio = "" + miProducto.getPrecio();
                    idIVA = "" + miProducto.getIdIVA();
                    notas = miProducto.getNotas();
                    costo = "" + miProducto.getCosto();
                    entrada = "" + miProducto.getEntrada();
                    estado = miProducto.getEstado();
                    fechaVencimiento = "" + miProducto.getFechaVencimiento();
                    foto = miProducto.getFoto();
                    mensaje = "Usuario Consultado...";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Limpiar
            if (limpiar) {
                idProducto = "";
                descripcion = "";
                precio = "";
                idIVA = "";
                notas = "";
                costo = "";
                entrada = "";
                estado = "";
                fechaVencimiento = "";
                foto = "";
                mensaje = "";
            }

            //Si presionan el boton Nuevo
            if (nuevo) {
                if (Utilidades.stringToFloat(precio) <= 0.0) {
                    mensaje = "Debe ingresar un valor Mayor a Cero en el Precio! \n :)";
                } else if (Utilidades.stringToFloat(costo) <= 0.0) {
                    mensaje = "Debe ingresar un valor numerico en el Costo! \n :)";
                } else if (Utilidades.stringToFloat(entrada) <= 0.0){
                    mensaje = "Debe ingresar un valor Mayor a Cero en La Entrada! \n :)";
                }else {
                    Datos misDatos = new Datos();
                    Producto miProducto = misDatos.getProducto(idProducto);
                    if (miProducto != null) {
                        mensaje = "Producto Ya Existe";
                    } else {
                        miProducto = new Producto(
                                idProducto,
                                descripcion,
                                new Float(precio),
                                new Integer(idIVA),
                                notas,
                                new Float(costo),
                                new Float(entrada),
                                estado,
                                Utilidades.stringToDate(fechaVencimiento),
                                foto);
                        misDatos.newProducto(miProducto);
                        mensaje = "Producto Ingresado";
                    }
                    misDatos.cerrarConexion();
                }

            }

            //Si presionan el boton Modificar
            if (modificar) {
                Datos misDatos = new Datos();
                Producto miProducto = misDatos.getProducto(idProducto);

                if (miProducto == null) {
                    mensaje = "Producto no Existe";
                } else {
                    miProducto = new Producto(
                            idProducto,
                            descripcion,
                            new Float(precio),
                            new Integer(idIVA),
                            notas,
                            new Float(costo),
                            new Float(entrada),
                            estado,
                            Utilidades.stringToDate(fechaVencimiento),
                            foto);
                    misDatos.updateProducto(miProducto);
                    mensaje = "Usuario Modificado";
                }
                misDatos.cerrarConexion();
            }

            //Si presionan el boton Listar
            if (listar) {
        %>
        <script>
            document.location.href = "ListadoProducto.jsp";
        </script>        
        <%
            }
        %>

        <h1>Productos</h1>
        <form name="productos" id="productos" action="Productos.jsp" method="POST">
            <table border="0">                
                <tbody>
                    <tr>
                        <td>Id Producto:</td>
                        <td><input type="text" name="idProducto" id="idProducto" value="<%=idProducto%>" size="24"/><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Descripcion:</td>
                        <td><input type="text" name="descripcion" id="descripcion" value="<%=descripcion%>" size="50"/><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Precio:</td>
                        <td><input onkeypress='return event.charCode >= 46 && event.charCode <= 57' type="text" name="precio" id="precio" value="<%=precio%>" size="10"/><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Impuesto</td>
                        <td><select name="idIVA" id="idIVA">
                                <option value="0" <%=(idIVA.equals("") ? "selected" : "")%>>Seleccione Impuesto Aplicar!</option>
                                <option value="1" <%=(idIVA.equals("1") ? "selected" : "")%>>0%</option>
                                <option value="2" <%=(idIVA.equals("2") ? "selected" : "")%>>16%</option>                                
                            </select> <font color="red">*</font> </td>
                    </tr>
                    <tr>
                        <td>Notas:</td>
                        <td><textarea name="notas" id="notas" rows="8" cols="50"><%=notas%></textarea></td>
                    </tr>
                    <tr>
                        <td>Costo:</td>
                        <td><input onkeypress='return event.charCode >= 46 && event.charCode <= 57' type="text" name="costo" id="costo" value="<%=costo%>" size="10" /><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Entrada:</td>
                        <td><input onkeypress='return event.charCode >= 46 && event.charCode <= 57' type="text" name="entrada" id="entrada" value="<%=entrada%>" size="10" /><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Estado:</td>
                        <td><select name="estado" id="estado">
                                <option value="0" <%=(estado.equals("") ? "selected" : "")%>>Seleccione Estado</option>
                                <option value="ACTIVO" <%=(estado.equals("ACTIVO") ? "selected" : "")%>>ACTIVO</option>
                                <option value="INACTIVO" <%=(estado.equals("INACTIVO") ? "selected" : "")%>>INACTIVO</option>                                
                            </select><font color="red">*</font></td>
                    </tr>
                    <tr>
                        <td>Fecha de Vencimiento:</td>
                        <td><input type="text" name="fechaVencimiento" id="fechaVencimiento" value="<%=fechaVencimiento%>" size="10" /></td>
                    </tr>
                    <tr>
                        <td>Foto:</td>
                        <td><input type="file" name="foto" id="foto" value="<%=foto%>" />
                            <%
                                if (!foto.equals("")) {
                            %>
                            <br>
                            <img src="<%="productos/" + foto%>" width="150" height="150"/>
                            <%
                            } else {
                            %>
                            <br>
                            <img src="images/lupa.png" width="150" height="150"/>
                            <%
                                }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" ><font color="red">* Campos Obligatorio</font></td>
                    </tr>
                </tbody>
            </table>
            <br>
            <jsp:include page="Botones.jsp"></jsp:include>
            </form>
            <br>
            <h1><%=mensaje%></h1>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>        
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>
    </body>
</html>