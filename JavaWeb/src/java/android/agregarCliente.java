package android;

import clases.Cliente;
import clases.Datos;
import clases.Utilidades;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
@WebService(serviceName = "agregarCliente")
public class agregarCliente {

    /**
     * Web service operation
     * @param PCliente
     * @return 
     */
    @WebMethod(operationName = "agregarCliente")
    public String agregarCliente(@WebParam(name = "PCliente") Cliente PCliente) {
        Datos miDato = new Datos();
        PCliente = new Cliente("01200893442", 1, "Jhironsel", "Diaz Almonte", 
                "Av. Sur edif.:40", "8095578012", 2, 
                Utilidades.stringToDate("07.04.2014"), 
                Utilidades.stringToDate("23.01.2018"),
                10000.00f, 0.00f);
        miDato.newCliente(PCliente);
        return "Cliente Agregado";
    }
}
