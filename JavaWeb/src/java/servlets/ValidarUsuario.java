package servlets;

import clases.Datos;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ValidarUsuario", urlPatterns = {"/ValidarUsuario"})

public class ValidarUsuario extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //Creamos Objeto de Conexion a la base de Datos
            Datos misDatos = new Datos();

            //Tomar los Valores del JSP
            String usuario = request.getParameter("usuario");
            String clave = request.getParameter("clave");

            out.print(misDatos.validarUsuario(usuario, clave));
            
            //Almacenar el usuario logueado en la sesion
            //Esto va a la pagina de MenuAdministrador
            HttpSession sesion = request.getSession(true);
            sesion.setAttribute("usuario", misDatos.getUsuario(usuario));
            
            //Cerramos conexion
            misDatos.cerrarConexion();
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}
