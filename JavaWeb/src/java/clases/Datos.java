package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Datos {

    private Connection cnn;

    public Datos() {
        try {
            Class.forName("org.firebirdsql.jdbc.FBDriver");
            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "123uasd");
            props.setProperty("charSet", "UTF-8");
            props.setProperty("roleName", "SINTRIGGERSSOFTSURENA");
            cnn = DriverManager.getConnection("jdbc:firebirdsql://localhost/"
                    + "sistemaDeBebida", props);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    public void cerrarConexion() {
        try {
            cnn.close();
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String validarUsuario(String usuario, String clave) {
        try {
            String sql = "select IDPERFIL from USUARIOS where "
                    + "idUsuario = '" + usuario + "' "
                    + "and clave = '" + clave + "'";

            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                return rs.getString("idPerfil");
            } else {
                JOptionPane.showMessageDialog(null, "Usuario no valido, Cod.:1");
                return "nok";
            }

        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Usuario no valido, Cod.:2");
            return "nok";
        }
    }

    public Usuario getUsuario(String idUsuario) {
        try {
            Usuario miUsuario = null;
            String sql = "select * from usuarios where "
                    + "idUsuario = '" + idUsuario + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                miUsuario = new Usuario(
                        rs.getString("idUsuario"),
                        rs.getString("nombres"),
                        rs.getString("apellidos"),
                        rs.getString("clave"),
                        rs.getInt("idPerfil"),
                        rs.getString("foto"));
            }

            return miUsuario;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Producto getProducto(String idProducto) {
        try {
            Producto miProducto = null;
            String sql = "select * from productos where "
                    + "idProducto = '" + idProducto + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                miProducto = new Producto(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getFloat(3),
                        rs.getInt(4),
                        rs.getString(5),
                        rs.getFloat(6),
                        rs.getFloat(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10));
            }
            return miProducto;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Cliente getCliente(String idCliente) {
        try {
            Cliente miCliente = null;
            String sql = "select * from clientes where "
                    + "idCliente = '" + idCliente + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                miCliente = new Cliente(
                        rs.getString("idCliente"),
                        rs.getInt("idTipo"),
                        rs.getString("nombres"),
                        rs.getString("apellidos"),
                        rs.getString("direccion"),
                        rs.getString("telefono"),
                        rs.getInt("idCiudad"),
                        rs.getDate("fechaNacimiento"),
                        rs.getDate("fechaIngreso"),
                        rs.getFloat("credito"),
                        rs.getFloat("deudaACtual"));
            }
            return miCliente;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getUsuarios() {
        try {
            String sql = "select * from usuarios";
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }
    public ResultSet getResultSet(String sql) {
        try {            
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public ResultSet getProducto() {
        try {
            String sql = "select * from productos";
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public ResultSet getDetalleFacturaTmp() {
        try {
            String sql = "select * from detalleFacturaTmp";
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public ResultSet getClientes() {
        try {
            String sql = "select * from clientes";
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public void newUsuario(Usuario miUsuario) {
        try {
            String sql = "insert into usuarios values('"
                    + miUsuario.getIdUsuario() + "','"
                    + miUsuario.getNombres() + "', '"
                    + miUsuario.getApellidos() + "', '"
                    + miUsuario.getClave() + "', "
                    + miUsuario.getIdPerfil() + ", '"
                    + miUsuario.getFoto() + "')";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void newProducto(Producto miProducto) {
        try {
            String sql = "insert into productos values ('"
                    + miProducto.getIdProducto() + "', '"
                    + miProducto.getDescripcion() + "', "
                    + miProducto.getPrecio() + ", "
                    + miProducto.getIdIVA() + ", '"
                    + miProducto.getNotas() + "', "
                    + miProducto.getCosto() + ", "
                    + miProducto.getEntrada() + ", '"
                    + miProducto.getEstado() + "', '"
                    + Utilidades.formatDate(miProducto.getFechaVencimiento()) + "', '"
                    + miProducto.getFoto() + "')";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void newCliente(Cliente miCliente) {
        try {
            String sql = "insert into clientes values('"
                    + miCliente.getIdCliente() + "', "
                    + miCliente.getIdTipo() + ", '"
                    + miCliente.getNombres() + "', '"
                    + miCliente.getApellidos() + "', '"
                    + miCliente.getDireccion() + "', '"
                    + miCliente.getTelefono() + "', "
                    + miCliente.getIdCiudad() + ", '"
                    + Utilidades.formatDate(miCliente.getFechaNacimiento()) + "', '"
                    + Utilidades.formatDate(miCliente.getFechaIngreso()) + "', "
                    + miCliente.getCredito() + ", "
                    + miCliente.getDeudaActual() + ")";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateUsuario(Usuario miUsuario) {
        try {
            String sql = "update usuarios set "
                    + "nombres ='" + miUsuario.getNombres() + "', "
                    + "apellidos ='" + miUsuario.getApellidos() + "', "
                    + "clave ='" + miUsuario.getClave() + "', "
                    + "idPerfil =" + miUsuario.getIdPerfil() + ", "
                    + "foto ='" + miUsuario.getFoto() + "' "
                    + "where idUsuario = '" + miUsuario.getIdUsuario() + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateProducto(Producto miProducto) {
        try {
            String sql = "update productos set "
                    + "descripcion ='" + miProducto.getDescripcion() + "', "
                    + "precio =" + miProducto.getPrecio() + ", "
                    + "idIVA =" + miProducto.getIdIVA() + ", "
                    + "notas ='" + miProducto.getNotas() + "', "
                    + "costo =" + miProducto.getCosto() + ", "
                    + "entrada =" + miProducto.getEntrada() + ", "
                    + "estado ='" + miProducto.getEstado() + "', "
                    + "fechaVencimiento ='" + Utilidades.formatDate(miProducto.getFechaVencimiento()) + "', "
                    + "foto ='" + miProducto.getFoto() + "' "
                    + "where IdProducto = '" + miProducto.getIdProducto() + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateCliente(Cliente miCliente) {
        try {
            String sql = "update clientes set "
                    + "idTipo =" + miCliente.getIdTipo() + ", "
                    + "nombres ='" + miCliente.getNombres() + "', "
                    + "apellidos ='" + miCliente.getApellidos() + "', "
                    + "direccion ='" + miCliente.getDireccion() + "', "
                    + "telefono ='" + miCliente.getTelefono() + "', "
                    + "idCiudad =" + miCliente.getIdCiudad() + ", "
                    + "fechaNacimiento ='" + Utilidades.formatDate(miCliente.getFechaNacimiento()) + "', "
                    + "fechaIngreso ='" + Utilidades.formatDate(miCliente.getFechaIngreso()) + "' "
                    + "where idCliente ='" + miCliente.getIdCliente() + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteUsuario(String idUsuario) {
        try {
            String sql = "delete from usuarios where idUsuario = '" + idUsuario + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteProducto(String idProducto) {
        try {
            String sql = "delete from productos where idProducto = '" + idProducto + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteDetalleFacturaTmp(String idProducto) {
        try {
            String sql = "delete from detalleFacturaTmp where idProducto = '" + idProducto + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void deleteDetalleFacturaTmp() {
        try {
            String sql = "delete from detalleFacturaTmp";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void deleteCliente(String idCliente) {
        try {
            String sql = "delete from clientes where idCliente = '" + idCliente + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String deuda() {
        try {
            String sql = "SELECT sum(`deudaActual`) as \"Deuda\" FROM `clientes`";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            DecimalFormat df = new DecimalFormat("#.##");
            return (rs.next()) ? df.format(rs.getFloat("deuda")) : "Limpio";
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public float getTotalCantidad() {
        try {
            float total = 0;
            String sql = "select sum(cantidad) as suma from detalleFacturaTmp";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                total = rs.getFloat("suma");
            }
            return total;
        } catch (SQLException ex) {
            Logger.getLogger("Error en este Metodo en Datos ==>getTotalValor()"
                    + Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public float getTotalValor() {
        try {
            float total = 0;
            String sql = "select sum(cantidad * precio) as suma from detalleFacturaTmp";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                total = rs.getFloat("suma");
            }
            return total;
        } catch (SQLException ex) {
            Logger.getLogger("Error en este Metodo en Datos ==>getTotalValor()"
                    + Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public void newDetalleFacturaTmp(DetalleFacturaTmp miDetalle) {
        try {
            //Identificamos si el producto ya se encuentra en el Detalle Temporal
            String sql = "select (1) from detalleFacturaTmp "
                    + "where idProducto = '" + miDetalle.getIdProducto() + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                //Si hay detalle, se Actualiza la cantidad
                sql = "update detalleFacturaTmp set "
                        + "cantidad = cantidad + " + miDetalle.getCantidad()
                        + " where idProducto = '" + miDetalle.getIdProducto() + "'";
            } else {
                //No existe en el detalle, se Ingresa nuevo
                sql = "insert into detalleFacturaTmp values('"
                        + miDetalle.getIdProducto() + "', '"
                        + miDetalle.getDescripcion() + "', "
                        + miDetalle.getPrecio() + ", "
                        + miDetalle.getCantidad() + ")";
            }
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int siguienteFactura() {
        try {
            int aux = 1;
            String sql = "select max(idFactura)as num from factura";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                aux = rs.getInt("num") + 1;
            }
            return aux;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    public void newFactura(int idFactura, String idCliente, Date fecha, String miUsuario) {
        try {
            String sql = "insert into factura values("
                    + idFactura + ", '"
                    + idCliente + "', '"
                    + Utilidades.formatDate(fecha) + "', '"
                    + miUsuario + "')";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void newDetalleFactura(int idFactura, int idLinea, String idProducto, 
            String descripcion, float precio, float cantidad) {
        try {
            String sql = "insert into detalleFactura values("
                    + idFactura + ", "
                    + idLinea + ", '"
                    + idProducto + "', '"
                    + descripcion + "', "
                    + precio + ", "
                    + cantidad +")";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

}
