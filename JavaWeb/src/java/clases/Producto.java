package clases;

import java.util.Date;

public class Producto {
    private String idProducto;
    private String descripcion;
    private Float precio;
    private int idIVA;
    private String notas;
    private Float costo;
    private Float entrada;
    private String estado;
    private Date fechaVencimiento;
    private String foto;

    public Producto(String idProducto, String descripcion, Float precio,
            int idIVA, String notas, Float costo, Float entrada, String estado,
            Date fechaVencimiento, String foto) {
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.precio = precio;
        this.idIVA = idIVA;
        this.notas = notas;
        this.costo = costo;
        this.entrada = entrada;
        this.estado = estado;
        this.fechaVencimiento = fechaVencimiento;
        this.foto = foto;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public int getIdIVA() {
        return idIVA;
    }

    public void setIdIVA(int idIVA) {
        this.idIVA = idIVA;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    public Float getCosto() {
        return costo;
    }

    public void setCosto(Float costo) {
        this.costo = costo;
    }

    public Float getEntrada() {
        return entrada;
    }

    public void setEntrada(Float entrada) {
        this.entrada = entrada;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
    
    
}
