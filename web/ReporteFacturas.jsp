<%@page import="java.sql.ResultSet"%>
<%@page import="clases.Datos"%>
<%@page import="clases.Reportes"%>
<%@page import="clases.Utilidades"%>
<%@page import="java.util.Date"%>
<%@page autoFlush="true" buffer="1094kb"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : ReporteFacturas
    Created on : 01/16/2015, 12:27:17 PM
    Author     : jhironsel
--%>

<%@page import="clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Sistema de Facturacion [Reporte de Facturas] </title>
        <jsp:include page="Encabezado.jsp"></jsp:include>
        </head>
        <body>
        <%
            //Para trabajar con la sesion del usuario actual
            HttpSession sesion = request.getSession();
            Usuario miUsuario = (Usuario) sesion.getAttribute("usuario");
            if (miUsuario == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            //Variable que Muestra los mensajes del sistema
            String mensaje = "";

            //Identificamos el boton que el usuario presiono
            boolean generar = false;

            if (request.getParameter("generar") != null) {
                generar = true;
            }
            //obtenemos el valor como fue llamado el formulario
            String fechaInicial = "";
            String fechaFinal = "";

            if (request.getParameter("fechaInicial") != null) {
                fechaInicial = request.getParameter("fechaInicial");
            }

            if (request.getParameter("fechaFinal") != null) {
                fechaFinal = request.getParameter("fechaFinal");
            }

            if (fechaInicial.equals("")) {
                fechaInicial = Utilidades.formatDate(new Date());
            }

            if (fechaFinal.equals("")) {
                fechaFinal = Utilidades.formatDate(new Date());
            }

            //Si presionan boton de generar
            if (generar) {
                String sql = "SELECT factura.idFactura, factura.idCliente,"
                        + "CONCAT(nombres,' ', apellidos) AS nombreCompleto, "
                        + "fecha, idLinea, idProducto, descripcion, precio, "
                        + "cantidad, CAST( precio * cantidad AS DECIMAL( 15, 2 "
                        + ") ) AS valor FROM factura INNER JOIN clientes ON "
                        + "clientes.idCliente = factura.idCliente INNER JOIN "
                        + "detalleFactura ON detalleFactura.idFactura = "
                        + "factura.idFactura"
                        + " WHERE fecha >= '"+fechaInicial+"' AND fecha <='"+fechaFinal+"'";
                Datos misDatos = new Datos();
                ResultSet rs = misDatos.getResultSet(sql);                
                Reportes.reporteFacturas(rs);
                misDatos.cerrarConexion();
        %>
        <jsp:forward page="Reporte/Reporte.pdf"></jsp:forward>
        <%
            }
        %>

        <h1>Reporte de Factura</h1><br>
        <script>
            $(document).ready(function () {
                $("#fechaInicial").datepicker({monthNamesShort: ["Enero",
                        "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                        "Diciembre"], dateFormat: "yy-mm-dd", changeYear: true,
                    changeMonth: true});
                $("#fechaFinal").datepicker({monthNamesShort: ["Enero",
                        "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                        "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre",
                        "Diciembre"], dateFormat: "yy-mm-dd", changeYear: true,
                    changeMonth: true});
            });
        </script>
        <form name="ReporteFacturas" id="ReporteFacturas" action="ReporteFacturas.jsp" method="POST">
            <table border="0">

                <tbody>
                    <tr>
                        <td>Fecha Incial:</td>
                        <td><input type="text" name="fechaInicial" id="fechaInicial" value="<%=fechaInicial%>" size="10" /></td>
                    </tr>
                    <tr>
                        <td>Fecha Final:</td>
                        <td><input type="text" name="fechaFinal" id="fechaFinal" value="<%=fechaFinal%>" size="10" /></td>
                    </tr>
                    <tr>
                        <td colspan="2" ><input type="submit" value="General Reporte" name="generar" id="generar"/></td>
                    </tr>
                </tbody>
            </table>

        </form>    
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>
        <%
            if (miUsuario.getIdPerfil() == 1) {
        %>            
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>
        <%
        } else {
        %>
        <a href="MenuEmpleado.jsp">Regresar al Menu</a><br>
        <%
            }
        %>        
    </body>
</html>
