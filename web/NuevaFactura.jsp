<%-- 
    Document   : NuevaFactura
    Created on : 01/27/2015, 01:30:40 AM
    Author     : jhironsel
--%>
<%@page import="clases.Reportes"%>
<%@page autoFlush="true" buffer="1094kb"%>
<%@page import="clases.DetalleFacturaTmp"%>
<%@page import="clases.Producto"%>
<%@page import="clases.Utilidades"%>
<%@page import="java.util.Date"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="clases.Datos"%>
<%@page import="clases.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <jsp:include page="Encabezado.jsp"></jsp:include>
            <title>Sistema de Facturacion [Nueva Facturas]</title>
            <script>
                $(document).ready(function () {
                    $("#adicionar").click(function () {
                        return validarAdicionar();
                    });

                    $(".elimanar").click(function () {
                        $("#borrado").val($(this).attr("href"));
                        $("<div></div>").html("Esta seguro de querer borrar el producto " + $("#borrado").val() + "?").
                                dialog({title: "Confirmacion", modal: true, buttons: [
                                        {
                                            text: "Si",
                                            click: function () {
                                                $(this).dialog("close");
                                                $.post("EliminarDetalleFacturaTmp", {idProducto: $("#borrado").val()}, function (Data) {
                                                    location.reload();
                                                });
                                            }

                                        },
                                        {
                                            text: "No",
                                            click: function () {
                                                $(this).dialog("close");
                                            }
                                        }
                                    ]
                                });
                        return false;
                    });

                    $("#grabar").click(function () {
                        return validarCliente();
                    });
                });

                function validarAdicionar() {
                    if (validarProducto()) {
                        return validarCantidad();
                    }
                    return false;
                }

                function validarProducto() {
                    if ($("#producto").val() === "0") {
                        $("<div></div>").html("Debe ingresar un ID de Producto!").
                                dialog({
                                    title: "Error de Validacion",
                                    modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarCliente() {
                    if ($("#cliente").val() === "0") {
                        $("<div></div>").html("Debe ingresar un Cliente!").
                                dialog({
                                    title: "Error de Validacion",
                                    modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }

                function validarCantidad() {
                    if ($("#cantidad").val() === "") {
                        $("<div></div>").html("Debe Digitar un Valor en Cantidad").
                                dialog({
                                    title: "Error de Validacion",
                                    modal: true, buttons:
                                            [
                                                {
                                                    text: "Ok",
                                                    click: function () {
                                                        $(this).dialog("close");
                                                    }
                                                }
                                            ]
                                });
                        return false;
                    }
                    return true;
                }


            </script>
        </head>
        <body>
        <%
            //Para trabajar con la sesion del usuario actual Evitar que Un 
            //Usuario que no sea administrador no entre
            HttpSession sesion = request.getSession();
            Usuario miUsuarioLogueado = (Usuario) sesion.getAttribute("usuario");
            if (miUsuarioLogueado == null) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            if (miUsuarioLogueado.getIdPerfil() != 1) {
        %>
        <jsp:forward page="index.jsp"></jsp:forward>
        <%
            }
            //Variable que muestra los mensaje del sistema
            String mensaje = "";
            //Abrimos Conexion a la base de Datos
            Datos misDatos = new Datos();

            //Identicar que Boton el Usuario Presiono
            boolean adicionar = false;
            boolean grabar = false;

            if (request.getParameter("adicionar") != null) {
                adicionar = true;
            }
            if (request.getParameter("grabar") != null) {
                grabar = true;
            }

            //Obtenemos los valores de los campo como fue llamdo el formulario un tuyo pero...
            String fecha = Utilidades.formatDate(new Date());
            String cliente = "";
            String producto = "";
            String cantidad = "";

            //Aqui obtenemos los valores de los campos
            if (request.getParameter("fecha") != null) {
                fecha = request.getParameter("fecha");
            }
            if (request.getParameter("cliente") != null) {
                cliente = request.getParameter("cliente");
            }
            if (request.getParameter("producto") != null) {
                producto = request.getParameter("producto");
            }
            if (request.getParameter("cantidad") != null) {
                cantidad = request.getParameter("cantidad");
            }

            if (adicionar) {
                if (Utilidades.stringToFloat(cantidad) <= 0) {
                    mensaje = "Debe Ingresar una Cantidad Mayor a Cero (0)";
                } else {
                    //Buscamos producto en al BD
                    Producto miProducto = misDatos.getProducto(producto);

                    //Crear el objeto de detalle Factura
                    DetalleFacturaTmp miDetalle = new DetalleFacturaTmp(
                            miProducto.getIdProducto(),
                            miProducto.getDescripcion(),
                            miProducto.getPrecio(),
                            new Float(cantidad));
                    //Adicionamos el detalle
                    misDatos.newDetalleFacturaTmp(miDetalle);

                    //Inicializamos Variable y colocamos mensajes
                    producto = "0";
                    cantidad = "";
                    mensaje = "Producto Agregado";
                }
            }
            //Si presionan el boton de grabar
            if (grabar) {
                if (misDatos.getTotalCantidad() <= 0) {
                    mensaje = "Debe agregar Detalles en la Factura para grabar!";
                } else {
                    //Obtenemos un numero de Factura
                    int numFac = misDatos.siguienteFactura();

                    //grabamos Registro en la tabla Factura
                    misDatos.newFactura(numFac, cliente, new Date(),
                            miUsuarioLogueado.getNombres());

                    //Grabamos el detalle de la factura
                    ResultSet detalle = misDatos.getDetalleFacturaTmp();
                    int i = 1;
                    while (detalle.next()) {
                        misDatos.newDetalleFactura(
                                numFac,
                                i,
                                detalle.getString(1),
                                detalle.getString(2),
                                detalle.getFloat(3),
                                detalle.getFloat(4));
                        i++;
                    }

                    //Borramos el detalle de factura temporal
                    misDatos.deleteDetalleFacturaTmp();

                    //Incializamos variable y ponemos mensaje
                    cliente = "";
                    mensaje = "Factura #" + numFac + " Creada con Exito";

                    //Mostramos el reporte de la Factura
                    String sql = "SELECT factura.idFactura, factura.idCliente,"
                            + "CONCAT(nombres,' ', apellidos) AS nombreCompleto, "
                            + "fecha, idLinea, idProducto, descripcion, precio, "
                            + "cantidad, CAST( precio * cantidad AS DECIMAL( 15, 2 "
                            + ") ) AS valor FROM factura INNER JOIN clientes ON "
                            + "clientes.idCliente = factura.idCliente INNER JOIN "
                            + "detalleFactura ON detalleFactura.idFactura = "
                            + "factura.idFactura"
                            + " WHERE factura.idFactura = " + numFac;

                    ResultSet rs = misDatos.getResultSet(sql);
                    Reportes.reporteFacturas(rs);
        %>
        <jsp:forward page="Reporte/Reporte.pdf"></jsp:forward>
        <%
                }
            }

        %>

        <h1>Nueva Factura</h1>
        <form name="nuevaFactura" id="nuevaFactura" action="NuevaFactura.jsp" method="POST">
            <table border="0">                
                <tbody>
                    <tr>
                        <td>Fecha: </td>
                        <td colspan="3">
                            <input type="text" name="fecha" 
                                   id="fecha" value="<%=fecha%>" 
                                   size="10" disabled="disabled"
                                   />
                        </td>

                    </tr>
                    <tr>
                        <td>Cliente: </td>
                        <td colspan="3">
                            <select name="cliente" id="cliente">
                                <option value="0">Seleccione un Cliente</option>
                                <%
                                    //Llenamos las Clases que estan con los datos de la base de Datos...
                                    ResultSet clientes = misDatos.getClientes();
                                    while (clientes.next()) {
                                %>
                                <option value="<%=clientes.getString(1)%>"
                                        <%=(clientes.getString(1).equals(cliente) ? "selected" : "")%>
                                        > 
                                    <%=clientes.getString(3)%> 
                                    <%=clientes.getString(4)%>
                                </option>
                                <%
                                    }
                                %>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Producto: </td>
                        <td colspan="3">
                            <select name="producto" id="producto" >
                                <option value="0">Seleccione un Prudcto</option>
                                <%
                                    //Llenamos las Clases que estan con los datos de la base de Datos...
                                    ResultSet productos = misDatos.getProducto();
                                    while (productos.next()) {
                                %>
                                <option value="<%=productos.getString(1)%>"
                                        <%=(productos.getString(1).equals(producto) ? "selected" : "")%>
                                        > <%=productos.getString(2)%> </option>
                                <%
                                    }
                                %>
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>Cantidad: </td>
                        <td>
                            <input onkeypress='return event.charCode >= 46 &&
                                            event.charCode <= 57'
                                   type="text" 
                                   name="cantidad" 
                                   id="cantidad" 
                                   value="" 
                                   size="10" 
                                   />
                        </td>
                        <td>
                            <input type="submit" value="Adicionar" 
                                   name="adicionar" id="adicionar"
                                   />
                        </td>
                        <td>
                            <input type="submit" value="Grabar Factura" 
                                   name="grabar" id="grabar" 
                                   />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <h2><%=mensaje%></h2>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>            
            <table border="1">
                <thead>
                    <tr>
                        <th>Id del Producto</th>
                        <th>Descripcion</th>
                        <th>Precio</th>
                        <th>Cantidad</th>
                        <th>Valor</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                        ResultSet detalle = misDatos.getDetalleFacturaTmp();
                        while (detalle.next()) {
                    %>
                    <tr>
                        <td> <a href="<%=detalle.getString(1)%>" class="elimanar"> Elimanar: <%=detalle.getString(1)%> </a> </td>
                        <td>  <%=detalle.getString(2)%> </td>
                        <td align="right" ><%=detalle.getFloat(3)%></td>
                        <td align="right" ><%=detalle.getFloat(4)%></td>
                        <td align="right" ><%=String.format("%.02f", detalle.getFloat(3) * detalle.getFloat(4))%> </td>
                    </tr>
                    <%
                        }
                    %>
                    <tr>
                        <td align="right" colspan="3">Total:</td>
                        <td align="right" ><%=String.format("%.02f", misDatos.getTotalCantidad())%> </td>
                        <td align="right" ><%=String.format("%.02f", misDatos.getTotalValor())%></td>
                    </tr>
                </tbody>
            </table>


        </form>
        <%
            misDatos.cerrarConexion();
        %>
        <br>
        <a href="javascript:history.back(1)">Regresar a la pagina Anterior</a><br>        
        <a href="MenuAdministrador.jsp">Regresar al Menu</a><br>

        <input type="hidden" id="borrado" />

    </body>
</html>
