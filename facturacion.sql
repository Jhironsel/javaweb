-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 30-01-2015 a las 16:01:31
-- Versión del servidor: 5.5.41-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `facturacion`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE IF NOT EXISTS `clientes` (
  `idCliente` varchar(13) NOT NULL,
  `idTipo` int(11) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `direccion` varchar(50) DEFAULT NULL,
  `telefono` varchar(14) DEFAULT NULL,
  `idCiudad` int(11) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `credito` float DEFAULT NULL,
  `deudaActual` float DEFAULT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idCliente`, `idTipo`, `nombres`, `apellidos`, `direccion`, `telefono`, `idCiudad`, `fechaNacimiento`, `fechaIngreso`, `credito`, `deudaActual`) VALUES
('01', 3, 'Cliente Generico Nombre', 'Y Apellidos', 'Cliente Generico Direccion', '0-000-000-0000', 1, '2013-11-05', '2015-01-29', 0, 0),
('011-0048599-3', 1, 'MANUEL EMILIO', 'FERNANDEZ RODRIGUEZ', 'JOSE CONTRERA CASA #24', '829-443-5933', 2, '1975-10-11', '2013-10-29', 5000, 0),
('012-0046244-8', 1, 'SILVIA SOPHIA', 'ALMONTE PEREZ', '27 FEBRERO', '809-135-4622', 3, '1978-10-02', '2013-10-25', 2345, 0),
('012-0068755-5', 1, 'PEDRO PABLO', 'GUZMAN GONZALES', 'CALLE DUARTE CASA NUMERO #49', '849-945-4009', 3, '1977-12-31', '2013-10-30', 3500, 0),
('012-0089344-1', 1, 'SOPHIA', 'DIAZ VARGAS', 'CALLE 27 DE FEBRERO', '809-975-2211', 2, '2006-01-22', '2013-10-22', 2000, 0),
('012-0089344-2', 1, 'JHIRONSEL', 'DIAZ ALMONTE', 'CIRCUNV. SUR EDIF. 41 APART.102, Crito Rey.', '809-975-3152', 1, '1984-04-07', '2013-10-22', 3765, 0),
('012-0089344-3', 1, 'GALFONCEL', 'DIAZ ALMONTE', 'CIRCUNV. SUR EDIF. 40 APART. 102', '809-975-3433', 3, '1986-06-13', '2013-10-24', 500.01, 76.99),
('012-0097346-8', 1, 'PERLA MARIA', 'CABRERA NIN', 'PUENTE DUARTE CASA #23', '849-699-8997', 3, '2013-11-01', '2013-11-01', 128.48, 371.52),
('022-0086955-3', 1, 'GUILLERMO', 'MORENO', 'SANCHEZ CASA #99', '809-245-4490', 4, '2013-10-29', '2013-10-29', 4000, 0),
('edesur', 3, 'Paola', 'ramirez', 'anacaona  numero 24', '809-659-2222', 4, '1977-11-01', '2013-11-11', 2000, 0),
('infotep', 3, 'Infotep Regional', 'San Juan', 'Salida de la Ciudad', '809-888-3333', 3, '1976-05-05', '2005-05-17', 1500, 0),
('Navidad', 3, 'Programador', 'Activo', 'No se Duerme con esto', '794-223-4454', 4, '1994-12-09', '2013-12-23', 0, 0),
('Urania Montas', 3, 'Centro Urania Montas', 'Montas', 'Colon a lado del Liceo Tec. PHU', '809-557-1223', 1, '2013-11-01', '2013-11-11', 4797.6, 202.4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleFactura`
--

CREATE TABLE IF NOT EXISTS `detalleFactura` (
  `idFactura` int(11) NOT NULL,
  `idLinea` int(11) NOT NULL,
  `idProducto` varchar(24) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `cantidad` float NOT NULL,
  PRIMARY KEY (`idFactura`,`idLinea`),
  KEY `idLinea` (`idLinea`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalleFactura`
--

INSERT INTO `detalleFactura` (`idFactura`, `idLinea`, `idProducto`, `descripcion`, `precio`, `cantidad`) VALUES
(1, 1, '1', 'Pantis', 35.22, 12.3),
(1, 2, '13', 'ENSALADA LIB. 1', 34, 1.5),
(1, 3, '14', 'AZUCAR PRIETA LIBRA', 23.5, 23.23),
(2, 1, '10', 'Queso Amarillo', 60, 23.23),
(3, 1, '7467634947812', 'Libro de Bachiller 4to', 2, 1.5),
(4, 1, '07560818250057', 'Articulo papel', 15, 1.5),
(5, 1, '10', 'Queso Amarillo', 60, 6),
(5, 2, '3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 23.23),
(6, 1, '10', 'Queso Amarillo', 60, 23.23),
(6, 2, '14', 'AZUCAR PRIETA LIBRA', 23.5, 6),
(7, 1, '14', 'AZUCAR PRIETA LIBRA', 23.5, 1),
(7, 2, '4', 'LOLA BIEN', 513, 1.3),
(8, 1, '13', 'ENSALADA LIB. 1', 34, 23.23),
(8, 2, '2', 'Jabon de Cuaba', 23, 1.5),
(9, 1, '11', 'PROBANDO CON ID ALFANUMERICOS', 233, 6),
(10, 1, '1', 'Pantis', 35.22, 1),
(11, 1, '14', 'AZUCAR PRIETA LIBRA', 23.5, 1.5),
(12, 1, '1', 'Pantis', 35.22, 1.5),
(13, 1, '11', 'PROBANDO CON ID ALFANUMERICOS', 233, 23.23),
(14, 1, '10', 'Queso Amarillo', 60, 1.5),
(15, 1, '10', 'Queso Amarillo', 60, 1.5),
(16, 1, '13', 'ENSALADA LIB. 1', 34, 1),
(17, 1, '10', 'Queso Amarillo', 60, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalleFacturaTmp`
--

CREATE TABLE IF NOT EXISTS `detalleFacturaTmp` (
  `idProducto` varchar(24) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `cantidad` float NOT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE IF NOT EXISTS `factura` (
  `idFactura` int(11) NOT NULL,
  `idCliente` varchar(14) NOT NULL,
  `fecha` date NOT NULL,
  `idUsuario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idFactura`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `idCliente`, `fecha`, `idUsuario`) VALUES
(1, '01', '2015-01-30', 'Jhironsel'),
(2, '01', '2015-01-30', 'Jhironsel'),
(3, '01', '2015-01-30', 'Jhironsel'),
(4, '011-0048599-3', '2015-01-30', 'Jhironsel'),
(5, '012-0089344-1', '2015-01-30', 'Jhironsel'),
(6, '012-0046244-8', '2015-01-30', 'Jhironsel'),
(7, '0', '2015-01-30', 'Jhironsel'),
(8, '011-0048599-3', '2015-01-30', 'Jhironsel'),
(9, '022-0086955-3', '2015-01-30', 'Jhironsel'),
(10, '011-0048599-3', '2015-01-30', 'Jhironsel'),
(11, '0', '2015-01-30', 'Jhironsel'),
(12, '0', '2015-01-30', 'Jhironsel'),
(13, '0', '2015-01-30', 'Jhironsel'),
(14, '0', '2015-01-30', 'Jhironsel'),
(15, '0', '2015-01-30', 'Jhironsel'),
(16, '0', '2015-01-30', 'Jhironsel'),
(17, '01', '2015-01-30', 'Jhironsel');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE IF NOT EXISTS `productos` (
  `idProducto` varchar(24) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `precio` float NOT NULL,
  `idIVA` int(11) NOT NULL,
  `notas` text,
  `costo` float NOT NULL,
  `entrada` float NOT NULL,
  `estado` varchar(8) NOT NULL,
  `fechaVencimiento` date DEFAULT NULL,
  `foto` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProducto`, `descripcion`, `precio`, `idIVA`, `notas`, `costo`, `entrada`, `estado`, `fechaVencimiento`, `foto`) VALUES
('048231294461', 'DVD-R de 4.7 GB 8X', 16, 2, '', 12, 25.78, 'ACTIVO', '2014-05-07', 'DVD-R.png'),
('07560818250057', 'Articulo papel', 15, 1, 'localizado en tal parte', 10, 37, 'ACTIVO', '2015-01-23', ''),
('1', 'Pantis', 35.22, 2, 'Pantis para Hombres', 33.32, 22, 'ACTIVO', '2013-12-19', ''),
('10', 'Queso Amarillo', 60, 2, 'Trabajo Casi Terminado', 49.55, 5344, 'INACTIVO', '2015-01-15', ''),
('11', 'PROBANDO CON ID ALFANUMERICOS', 233, 0, '	FFFF', 23, 3, 'INACTIVO', '2015-03-20', ''),
('13', 'ENSALADA LIB. 1', 34, 1, 'PREPARAR', 23, 324, 'INACTIVO', '2015-01-28', ''),
('14', 'AZUCAR PRIETA LIBRA', 23.5, 2, 'AZUCAR POR UN TUVO modificado para probar', 19.44, 150.2, 'INACTIVO', '2015-01-31', ''),
('2', 'Jabon de Cuaba', 23, 2, 'PRODUCTO PARA EVALUAR SISTEMA....', 20, -38.8, 'ACTIVO', '2015-01-30', ''),
('3', 'PASTA DENTAL COLGATE TANTO GRAMOS', 175, 1, 'PRODUCTO DE PRUEBAS', 145, 16, 'ACTIVO', '2015-01-31', ''),
('4', 'LOLA BIEN', 513, 1, 'ALGO DE NOTAS PARA PROBAR	2', 450.3, 6, 'ACTIVO', '2015-01-20', ''),
('7', 'PRODUCTO CREADO POR DATOS FLOTANTES', 34.44, 0, 'ESTO SE IVA A QUEDAR EN BLANCO', 32.45, 30, 'ACTIVO', '2015-07-30', ''),
('7467634947812', 'Libro de Bachiller 4to', 2, 0, 'Mostrador de Alante', 1, 9, 'ACTIVO', '2015-05-29', ''),
('7501015213827', 'Marcador de Agua Verde', 15, 1, 'Marcador verde de NIN', 8, 69, 'ACTIVO', '2015-02-12', ''),
('7501056330255', 'ponds cont. neto 50g', 100, 1, 'localizado en el mostrador del negocio.\n\n\n', 85, 18, 'ACTIVO', '2015-01-23', ''),
('9', 'HARINA DEL NEGRITO', 43.44, 0, 'PREPARAR HARINA', 32, 324, 'INACTIVO', '2015-01-31', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provedor`
--

CREATE TABLE IF NOT EXISTS `provedor` (
  `idProvedor` varchar(13) NOT NULL,
  `nombres` varchar(25) NOT NULL,
  `telefono` varchar(12) NOT NULL,
  PRIMARY KEY (`idProvedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idUsuario` varchar(13) NOT NULL,
  `nombres` varchar(30) NOT NULL,
  `apellidos` varchar(30) NOT NULL,
  `clave` varchar(10) NOT NULL,
  `idPerfil` int(11) NOT NULL,
  `foto` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombres`, `apellidos`, `clave`, `idPerfil`, `foto`) VALUES
('Jhironsel', 'Jhironsel', 'Diaz Almonte', '123', 1, 'Jhironsel.jpg'),
('Jhironsel2', 'Yironsel', 'Perez Almonte', '123', 2, 'Silvia.png'),
('Sophia', 'Sophia', 'Diaz Vargas', '123', 1, 'SOphia.jpg'),
('xxx', 'xxx', 'xxx', 'xxx', 2, ''),
('xxx2', 'xxx', 'xxx', 'xxx', 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
