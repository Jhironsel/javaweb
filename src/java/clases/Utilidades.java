package clases;

import static java.lang.Float.parseFloat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilidades {
    public static String formatDate(Date fecha){
        if(fecha == null){
            fecha = new Date();
        }
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        return formatoDelTexto.format(fecha);
    }
    public static Date stringToDate(String fecha){
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        Date aux = null;
        try{
            aux = formatoDelTexto.parse(fecha);
        }catch (Exception ex){
            
        }
        return aux;
    }
    
    public static boolean isNumeric(String cadena){
        try{
            Integer.parseInt(cadena);
            return true;
        } catch(NumberFormatException nfe){
            return false;
        }
    }
    
    public static boolean isNumeric2(String cadena){
        try{
            Float.parseFloat(cadena);
            return true;
        } catch(NumberFormatException nfe){
            return false;
        }
    }
    
    public static int stringToInt(String cadena){
        int aux = 0;
        try{
            aux = Integer.parseInt(cadena);
            return aux;
        } catch(NumberFormatException nfe){
            return aux;
        }
    }
    
    public static double stringToDouble(String cadena){
        double aux = 0;
        try{
            aux = Double.parseDouble(cadena);
            return aux;
        } catch(NumberFormatException nfe){
            return aux;
        }
    }
    public static float stringToFloat(String cadena){
        float aux = 0;
        try{            
            aux = Float.parseFloat(""+parseFloat(cadena));
            return aux;
        } catch(NumberFormatException nfe){
            return aux;
        }
    }
    
}
