package clases;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Reportes {

    //Lo Basico
//    public static void reporteFacturas(ResultSet rs){
//        Document documento = new Document();
//        try {            
//            PdfWriter.getInstance(documento, new FileOutputStream("/media/"
//                    +"jhironsel/Almacen de 465Gb/Users/Jhironsel/Documents/"
//                    +"NetBeansProjects/JavaWeb/build/web/Reporte/Reporte.pdf"));
//            documento.open();
//            
//            //Colocamos titulo al reporte
//            String texto = "Reporte Facturas";
//            Paragraph parrafo = new Paragraph(texto);
//            documento.add(parrafo);
//            
//        } catch (Exception ex) {
//            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            documento.close();
//        }
//    }
    public static void reporteFacturas(ResultSet rs) {
        Document documento = new Document();
        try {
            PdfWriter.getInstance(documento, new FileOutputStream("/media/"
                    + "jhironsel/Almacen de 465Gb/Users/Jhironsel/Documents/"
                    + "NetBeansProjects/JavaWeb/build/web/Reporte/Reporte.pdf"));
            documento.open();

            //Colocamos titulo al reporte
            String texto = "Reporte Facturas";
            Paragraph parrafo = new Paragraph(texto);
            documento.add(parrafo);
            texto = " ";
            parrafo = new Paragraph(texto);
            documento.add(parrafo);

            float totGenCan = 0;
            float totGenVal = 0;
            float totFacCan = 0;
            float totFacVal = 0;

            //Leemos registro del Resulset
            boolean hayRegistros = rs.next();
            PdfPTable tabla;

            //Ciclo que recorre el ResultSet
            while (hayRegistros) {
                //Colocamos encabezado de la factura
                tabla = new PdfPTable(2);
                tabla.addCell("Id Factura: ");
                tabla.addCell(rs.getString("idFactura"));
                tabla.addCell("Id Cliente: ");
                tabla.addCell(rs.getString("idCliente"));
                tabla.addCell("Nombre: ");
                tabla.addCell(rs.getString("nombreCompleto"));
                tabla.addCell("Fecha: ");
                tabla.addCell(rs.getString("fecha"));
                tabla.addCell("");

                parrafo = new Paragraph();
                parrafo.add(tabla);
                documento.add(parrafo);

                tabla = new PdfPTable(6);
                tabla.addCell("Id Linea");
                tabla.addCell("Id Producto");
                tabla.addCell("Descripcion");
                tabla.addCell("Precio");
                tabla.addCell("Cantidad");
                tabla.addCell("Valor");

                //Inicializamos Totales de Factura
                totFacCan = 0;
                totFacVal = 0;

                int facturaActual = rs.getInt("idFactura");

                //Adicionamos el Detalle de la Factura                
                while (hayRegistros && facturaActual == rs.getInt("idFactura")) {

                    tabla.addCell(rs.getString("idLinea"));
                    tabla.addCell(rs.getString("idProducto"));
                    tabla.addCell(rs.getString("descripcion"));
                    tabla.addCell(rs.getString("precio"));
                    tabla.addCell(rs.getString("cantidad"));
                    tabla.addCell(rs.getString("valor"));

                    totFacCan += rs.getFloat("cantidad");
                    totFacVal += rs.getFloat("valor");

                    hayRegistros = rs.next();
                }

                tabla.addCell(" ");
                tabla.addCell(" ");
                tabla.addCell(" ");
                tabla.addCell("Total:");
                tabla.addCell("" + totFacCan);
                tabla.addCell("" + totFacVal);

                totGenCan += totFacCan;
                totGenVal += totFacVal;

                parrafo = new Paragraph();
                parrafo.add(tabla);
                documento.add(parrafo);

                texto = " ";
                parrafo = new Paragraph(texto);
                documento.add(parrafo);
            }            

            tabla = new PdfPTable(6);

            tabla.addCell(" ");
            tabla.addCell(" ");
            tabla.addCell(" ");
            tabla.addCell("Total General:");
            tabla.addCell("" + totGenCan);
            tabla.addCell("" + totGenVal);
            
            parrafo = new Paragraph();
            parrafo.add(tabla);
            documento.add(parrafo);
            
        } catch (Exception ex) {
            Logger.getLogger(Reportes.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            documento.close();
        }
    }
}
